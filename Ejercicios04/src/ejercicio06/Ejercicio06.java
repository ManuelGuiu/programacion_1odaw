package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		
		System.out.println("Inroduce un numero decimal");
		float decimal1 = input.nextFloat();
		System.out.println("Introduce otro m�s");
		float decimal2 = input.nextFloat();
		input.nextLine();
		
		System.out.println("Elegir uno de los siguientes caracteres: +, -, *, /, %");
		char operador = input.nextLine().charAt(0);
		
		switch(operador) {
			case '+':
				System.out.println(decimal1+decimal2);
				break;
			case '-':
				System.out.println(decimal1-decimal2);
				break;
			case '*':
				System.out.println(decimal1*decimal2);
				break;
			case '/':
				System.out.println(decimal1/decimal2);
				break;
			case '%':
				System.out.println(decimal1%decimal2);
				break;
			default:
				System.out.println("Caracter introducido no v�lido");
		}
		input.close();
	}

}
