package ejercicio01;

import java.util.Scanner;

public class Ejercicio01b {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		
		System.out.println("Introduce una vocal minuscula");
		char caracter = input.nextLine().charAt(0);
		
		switch(caracter) {
			case 'a':
				System.out.println("Has introducido la vocal "+caracter);
				
			case 'e':
				System.out.println("Has introducido la vocal "+caracter);
			
			case 'i':
				System.out.println("Has introducido la vocal "+caracter);

			case 'o':
				System.out.println("Has introducido la vocal "+caracter);
				
			case 'u':
				System.out.println("Has introducido la vocal "+caracter);
				
			default:
				System.out.println(caracter+" No es una vocal minuscula");
		}
		System.out.println("El programa mostrar� todos los casos a partir del seleccionado");
		input.close();
	}

}