package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		
		System.out.println("Introduce una vocal minuscula");
		char caracter = input.nextLine().charAt(0);
		
		switch(caracter) {
			case 'a':
				System.out.println("Has introducido la vocal "+caracter);
				break;
			case 'e':
				System.out.println("Has introducido la vocal "+caracter);
				break;
			case 'i':
				System.out.println("Has introducido la vocal "+caracter);
				break;
			case 'o':
				System.out.println("Has introducido la vocal "+caracter);
				break;
			case 'u':
				System.out.println("Has introducido la vocal "+caracter);
				break;
			default:
				System.out.println(caracter+" No es una vocal minuscula");
		}
		
		input.close();
	}

}
