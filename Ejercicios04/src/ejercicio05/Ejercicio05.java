package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		
		System.out.println("Introduce un mes en minusculas");
		String mes = input.nextLine();
		
		switch(mes) {
			case "enero":
				
			case "marzo":
				
			case "mayo":
				
			case "julio":
				
			case "agosto":
				
			case "octubre":
				
			case "diciembre":
				System.out.println(mes+" Tiene 31 d�as");
				break;
			case "febrero":
				System.out.println(mes+" Tiene 28 d�as");
				break;
			case "abril":
				
			case "junio":
				
			case "septiembre":
				
			case "noviembre":
				System.out.println(mes+" Tiene 30 d�as");
				break;
			default:
				System.out.println("El valor introducido es incorrecto");
			
		}
		input.close();
	}

}
