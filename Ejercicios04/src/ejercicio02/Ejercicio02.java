package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		
		System.out.println("Introduce una cantidad en gramos:");
		double gramos = input.nextDouble();
		
		if(gramos>=0) {
		
		System.out.println("Elige una de las conversiones");
		System.out.println("1-Kilogramo");
		System.out.println("2-Hectogramos");
		System.out.println("3-Decagramos");
		System.out.println("4-Decigramos");
		System.out.println("5-Centigramos");
		System.out.println("6-Miligramos");
		int menu = input.nextInt();
		
		switch (menu){
			case 1:
				System.out.println(gramos/1000+"kg");
				break;
			case 2:
				System.out.println(gramos/100+"hg");
				break;
			case 3:
				System.out.println(gramos/10+"dag");
				break;
			case 4:
				System.out.println(gramos*10+"dg");
				break;
			case 5:
				System.out.println(gramos*100+"cg");
				break;
			case 6:
				System.out.println(gramos*1000+"mg");
				break;
			default:
				System.out.println("Valor de menu incorrecto");
		}
		}else {
			System.out.println("Los gramos tienen que ser mayor o igual a 0");
		}
		
		input.close();
	}

}
