package ejercicio03;

public class Ejercicio03 {

	/*
	 * Crear un programa que muestra un array de caracteres. Desde el m�todo main se
	 * construye y rellena un array con las letras desde la �a� a la �k�. No
	 * aleatoriamente, sino que la posici�n 0 del array tendr� la �a�, la posici�n 1
	 * tendr� la �b�, y as� sucesivamente hasta la �k�. Posteriormente se llamar� a
	 * un m�todo que recibe el array como par�metro, modifica cada vocal por el
	 * car�cter �*� y lo devuelve al programa principal. Despu�s el programa
	 * principal mostrar� el contenido del array que ha devuelto el m�todo.
	 */

	public static void main(String[] args) {
		char v[] = new char[11];

		for (int i = 'a', j = 0; j < v.length; i++, j++) {
			v[j] = (char) i;
			System.out.print(v[j] + " ");
		}
		
		System.out.println();
		
		v = cambio(v);
		for(int i = 0; i < v.length; i++) {
			System.out.print(v[i]);
			System.out.print(" ");
		}
	}

	public static char[] cambio(char[] vector) {
		for (int i = 0; i < vector.length; i++) {
			switch (vector[i]) {
			case 'a':
				vector[i] = '*';
				break;
			case 'e':
				vector[i] = '*';
				break;
			case 'i':
				vector[i] = '*';
				break;
			case 'o':
				vector[i] = '*';
				break;
			case 'u':
				vector[i] = '*';
				break;
			}
		}
		return vector;
	}

}
