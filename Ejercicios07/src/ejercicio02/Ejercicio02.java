package ejercicio02;

public class Ejercicio02 {

	/*
	 * Programa que muestra 10 n�meros almacenados en un array. En este ejercicio
	 * desde el m�todo main creamos y rellenamos un array de 10 elementos con
	 * n�meros aleatorios de 4 a 15 (incluyendo ambos). Posteriormente se llamar� a
	 * un m�todo, que recibir� dicho array y solamente lo mostrar� por consola (el
	 * m�todo no devuelve nada).
	 */

	public static void main(String[] args) {
		int v[] = new int[10];

		for (int i = 0; i < v.length; i++) {
			v[i] = (int) (Math.random() * (4 - 19) + 19);
		}

		lector(v);
	}

	static void lector(int[] vector) {
		for (int i = 0; i < vector.length; i++) {
			System.out.println(vector[i]);
		}
	}

}
