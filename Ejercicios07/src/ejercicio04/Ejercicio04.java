package ejercicio04;

import java.util.Scanner;
import ejercicio03.Ejercicio03;

public class Ejercicio04 {

	/*
	 * Crear un programa parecido al anterior. El programa desde el m�todo main debe
	 * pedirnos por teclado el n�mero de elementos del array, y posteriormente
	 * construye un array y lo rellene con letras aleatorias de la �a� a la �z�.
	 * Mostrar� el array por pantalla. Despu�s, debemos llamar al m�todo del
	 * ejercicio anterior (debe ser p�blico) que modificar� nuestro array y nos lo
	 * devolver�. Posteriormente lo mostraremos por pantalla.
	 */

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce la longitud del vector:");
		int longitud = input.nextInt();

		char v[] = new char[longitud];
		
		for(int i = 0; i < v.length; i++) {
			v[i] = (char) (Math.random() * (97 - 122) + 122);
			System.out.print(v[i] + " ");
		}
		
		System.out.println();

		v=Ejercicio03.cambio(v);
		for(int i = 0; i < v.length; i++) {
			System.out.print(v[i] + " ");
		}

		input.close();
	}

}
