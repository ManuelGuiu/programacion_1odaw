package com.guiurm.sobrecarga;

import java.util.Scanner;

public class Operacion {
	
	static Scanner input = new Scanner (System.in);
	
	public static int lectorEntero() {
		System.out.println("Introduce un numero entero");
		int num = input.nextInt();
		return num;
	}
	
	public static double lectorDouble() {
		System.out.println("Introduce un numero double");
		double num = input.nextDouble();
		return num;
	}
	
	public static int suma(int n1, int n2) {
		int sum = n1 + n2;
		return sum;
	}
	
	public static double suma(double n1, double n2) {
		double sum = n1 + n2;
		return sum;
	}
	
	public static double suma(double n1, double n2, double n3) {
		double sum = n1 + n2 + n3;
		return sum;
	}
	
	public static int suma(int n1, int n2, int n3, int n4) {
		int sum = n1 + n2 + n3 + n4;
		return sum;
	}
	
	public static double suma(double n1, double n2, double n3, double n4) {
		double sum = n1 + n2 + n3 + n4;
		return sum;
	}

	public static void main(String[] args) {
		
		System.out.println(Operacion.suma(Operacion.lectorEntero(), Operacion.lectorEntero()));
		System.out.println(Operacion.suma(Operacion.lectorDouble(), Operacion.lectorDouble()));
		
		System.out.println(Operacion.suma(Operacion.lectorEntero(), Operacion.lectorEntero(), Operacion.lectorEntero()));
		System.out.println(Operacion.suma(Operacion.lectorDouble(), Operacion.lectorDouble(), Operacion.lectorDouble()));
		
		System.out.println(Operacion.suma(Operacion.lectorEntero(), Operacion.lectorEntero(), Operacion.lectorEntero(), Operacion.lectorEntero()));
		System.out.println(Operacion.suma(Operacion.lectorDouble(), Operacion.lectorDouble(), Operacion.lectorDouble(), Operacion.lectorDouble()));
		
	}

}
