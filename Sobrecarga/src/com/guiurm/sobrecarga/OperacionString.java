package com.guiurm.sobrecarga;

import java.util.Scanner;

public class OperacionString {
	
	static Scanner input = new Scanner (System.in);
	static String cad;
	
	public static String cambio1() {
		String cadena = OperacionString.lector();
		String subCadena = cadena.substring(0, cadena.length() - 1);
		
		return subCadena;
	}
	
	public static String lector() {
		System.out.println("Introduce una cadena");
		String cad = input.nextLine();
		return cad;
	}

	public static void main(String[] args) {
		System.out.println(OperacionString.cambio1());
	}

}
