package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		System.out.println("Introduce un numero");
		int numero = input.nextInt();
		int i = 0;
		if (numero > 0) {
			do {
				numero = numero / 10;
				i = i + 1;
			}while(numero >= 0);
			System.out.println(i);
			
		}else if(numero == 0){
			System.out.println("1");
		}else {System.out.println("El numero es negativo");}
		
		input.close();

	}

}
