package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {
	static final String FIN = "Fin del programa";
	
	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		System.out.println("Introuce un numero");
		int num1 = input.nextInt();
		System.out.println("Introduce otro numero");
		int num2 = input.nextInt();
		
		if ( num1 > num2 ) {
			
			for (int i = num1; i > num2; i--) {
				System.out.println(i);
			}
			
			for (int i = num1; i > num2; i--) {
				System.out.print(i + " ");
			}
			System.out.println();
			
		}else {
			System.out.println("El primero es menor que el segundo");
		}
		
		System.out.println(FIN);
		
		input.close();
	}

}
