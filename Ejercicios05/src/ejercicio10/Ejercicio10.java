package ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		double suma = 0;
		
		for(int i = 0; i < 5; i++) {
			System.out.println("Introuce un numero");
			double numero = input.nextDouble();
			suma = numero + suma;
		}
		System.out.println(suma);
		
		input.close();

	}

}
