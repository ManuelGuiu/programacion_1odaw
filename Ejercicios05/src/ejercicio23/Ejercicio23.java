package ejercicio23;

import java.util.Scanner;

public class Ejercicio23 {

	public static void main(String[] args) {
		/*Programa que pide un n�mero binario de 8 cifras y muestra su conversi�n a sistema decimal.
		Debemos crear un algoritmo que haga eso, sin usar clases nuevas. Solo tenemos que
		recordar las operaciones que debo hacer para pasar de binario a decimal, y encontrar el
		patr�n que se repite. Despu�s representar esa repetici�n mediante un bucle.*/
		Scanner input = new Scanner (System.in);
		
		System.out.println("Introduce un numero binario de 8 cifras");
		String binario = input.nextLine();
		int potencia = binario.length() - 1;
		int suma = 0;
		int contador = 0;
		
		for(int i = 0; i < binario.length(); i++) {
			if (binario.charAt(i) != '0' && binario.charAt(i) != '1') {
				contador++;
			}
		}
		if(contador > 0) {
			System.out.println("Introduce un numero binario");
		}else {
			for (int i = 0; i < binario.length(); i++) {
				int digito = binario.charAt(i) - 48;
				
				suma = (int) (suma + (digito * Math.pow(2, potencia)));
				potencia--;
			}
			System.out.println(suma);
		}
		
		input.close();
	}

}
