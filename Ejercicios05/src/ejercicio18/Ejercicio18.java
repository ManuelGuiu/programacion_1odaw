package ejercicio18;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		/*Programa para controlar el nivel de un dep�sito de agua. El programa inicialmente nos
pedir� el volumen total en litros del dep�sito. Posteriormente nos pide constantemente la
cantidad de litros que metemos al dep�sito o que retiramos (n�meros positivos echamos,
negativos retiramos). Despu�s de cada cantidad que introducimos, nos debe mostrar la
cantidad restante de litros que hace falta para llenarlo completamente. El programa deja de pedirnos cantidades cuando se ha llegado o superado el l�mite del dep�sito, indic�ndonoslo
por pantalla.*/
		
		Scanner input = new Scanner (System.in);
		
		System.out.println("Introduce la cantidad de litros del deposito");
		int capacidad = input.nextInt();
		int cantidad = 0;
		int resto = 0;
		do {
			System.out.println("Litros a�adidos o retirados");
			int litrosMovidos = input.nextInt();
			resto = cantidad + litrosMovidos + resto;
			if(resto > capacidad) {
				System.out.println("Deposito lleno");
			}else if(resto < 0) {
				System.out.println("Deposito vacio");
			}else {
				System.out.println(resto);
			}
		}while(resto > 0 && resto < capacidad);
		
		input.close();

	}

}
