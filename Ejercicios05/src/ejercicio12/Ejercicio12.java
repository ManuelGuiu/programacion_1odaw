package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		int numero = 0;
		do {
			System.out.println("*********************************\r\n" + 
					"1 � Opcion 1\r\n" + 
					"2 - Opcion 2\r\n" + 
					"3 � Salir\r\n" +  
					"*********************************\r\n" + 
					"Introduzca el n�mero de opci�n:");
			numero = input.nextInt();
			
			switch(numero){
			case 1:
				System.out.println("Opcion 1");
				break;
			case 2:
				System.out.println("Opcion 2");
				break;
			case 3:
				System.out.println("Fin del programa");
				break;
			default:
				System.out.println("Introduce una de las 3 opciones");
			}
		}while(numero != 3);
		
		input.close();
	}

}
