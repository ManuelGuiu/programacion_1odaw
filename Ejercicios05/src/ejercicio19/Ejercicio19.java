package ejercicio19;

import java.util.Scanner;

public class Ejercicio19 {

	public static void main(String[] args) {
		/* Programa para corregir ex�menes. Se introducen las notas del alumno de cada examen
		(double) hasta que se introduzca un n�mero negativo, y calcular la nota media.*/
		
		Scanner input = new Scanner (System.in);
		double nota = 0;
		double media = 0;
		double contador = 0;
		
		while(nota >= 0) {
			System.out.println("Introduce una nota");
			nota = input.nextDouble();
			if (nota >= 0) {
				media = media + nota;
				contador++;
			}
		}
		System.out.println( "La noa media del alumno es " + media / contador);
		input.close();
	}

}
