package ejercicio16;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);

		int numero = 0;
		int contador = 0;
		for (int i = 1; i <= 10; i++) {
			System.out.println("Introduce el numero " + i);
			numero = input.nextInt();
			if(numero < 0) {
				contador++;
			}
		}System.out.println("Se han introducido " + contador + " numeros negativos");
		
		
		input.close();

	}

}
