package ejercicio07;

import java.util.Scanner;

public class Ejercicio07 {
	
	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		System.out.println("Introduce un numero");
		int numero = input.nextInt();
		
		int multiplo = 0;
		if(numero >=0 && numero <= 10) {
		for (int i = numero; i <= numero * 10; i = numero * multiplo ) {
			
			
			System.out.println(i);
			multiplo++;
		}
		}else {
			System.out.println("El numero tiene que estar entre 0 y 10");
			}
		input.close();

	}

}
