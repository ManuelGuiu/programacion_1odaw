package ejercicio02;

public class Ejercicio02 {
	static final String FIN = "Fin del programa";
	public static void main(String[] args) {
		int num = 100;
		do {
			System.out.println(num);
			num --;
		} while( num >= 1);
		
		num = 100;
		do {
			System.out.print(num + " ");
			num --;
		} while( num >= 1);
		System.out.println();
		System.out.println(FIN);
	}

}
