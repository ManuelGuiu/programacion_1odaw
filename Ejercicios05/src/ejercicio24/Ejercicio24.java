package ejercicio24;

import java.util.Scanner;

public class Ejercicio24 {

	public static void main(String[] args) {
		/* Programa que pida cadenas de texto hasta introducir la cadena de texto �fin�.
		Posteriormente me dir� la cantidad de vocales (may�sculas o min�sculas) y el porcentaje de
		vocales respecto al total de caracteres. Adem�s, me mostrar� la cadena resultante de unir
		todas las cadenas separadas por un espacio*/
		Scanner input = new Scanner (System.in);
		String cadena = "";
		double contadorVocales = 0;
		double contadorCaracter = 0;
		String cadenaLarga = "";
		double porcentaje = 0;
		
		do {
			System.out.println("Introduce una cadena");
			cadena = input.nextLine();
			for(int i = 0; i < cadena.length(); i++) {
				if(cadena.toLowerCase().charAt(i) == 'a' || cadena.toLowerCase().charAt(i) == 'e' || cadena.toLowerCase().charAt(i) == 'i' || cadena.toLowerCase().charAt(i) == 'o' || cadena.toLowerCase().charAt(i) == 'u') {
					contadorVocales++;
				}
				contadorCaracter++;
			}
			cadenaLarga = cadenaLarga + " " + cadena;
		}while(!cadena.equals("fin"));
		porcentaje = (contadorVocales * 100) / contadorCaracter;
		System.out.println("El numero de vocales total es " + contadorVocales);
		System.out.println("El porcentaje de vocales respecto a el resto de caracteres es " + porcentaje);
		System.out.println(cadenaLarga);
		input.close();
	}

}
