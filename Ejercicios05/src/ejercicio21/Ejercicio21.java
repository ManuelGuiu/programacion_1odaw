package ejercicio21;

import java.util.Scanner;

public class Ejercicio21 {

	public static void main(String[] args) {
		/*Pasar de grados cent�grados a grados kelvin. El proceso de leer grados cent�grados y
		convertirlos se debe repetir mientras que se responda �S� a la pregunta: �Repetir proceso?
		(S/N). (Conversi�n: �K = �C + 273)*/
		
		Scanner input = new Scanner (System.in);
		String respuesta = "S";

		do {
			double centigrado = 0;
			double kelvin = 0;
			System.out.println("Introduce los centigrados");
			centigrado = input.nextDouble();
			input.nextLine();
			kelvin = centigrado - 273;
			System.out.println("En kelvin son " + kelvin + "�K");
			System.out.println("�Repetir proceso?");
			System.out.println("S/N");
			respuesta = input.nextLine();
			
		}while(!respuesta.equals("N"));
		
		input.close();
	}

}
