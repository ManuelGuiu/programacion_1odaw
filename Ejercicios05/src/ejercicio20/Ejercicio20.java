package ejercicio20;

import java.util.Scanner;

public class Ejercicio20 {

	public static void main(String[] args) {
		/*Programa que pida el largo y el ancho (valores enteros) y dibuje un rect�ngulo, con tantas
		�X� por alto y por ancho como se han introducido por teclado*/
		Scanner input = new Scanner (System.in);
		
		System.out.println("Introduce el ancho del cuadrado");
		int y = input.nextInt();
		System.out.println("Introduce el largo del cuadrado");
		int x = input.nextInt();
		
		for(int i = 0; i < x; i++) {
			
			for(int j = 0; j < y; j++) {
				System.out.print("X ");
			}
			System.out.println();
		}
		
		input.close();
	}

}
