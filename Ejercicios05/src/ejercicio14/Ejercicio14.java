package ejercicio14;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
		/*Programa que pide n�meros enteros positivos y negativos por teclado hasta que
		introducimos el n�mero 0. Despu�s deber� indicar el n�mero total de n�meros le�dos y la
		suma total de sus valores.*/
		int numero= 1;
		int guardado = 0;
		int contador = 0;
		Scanner input = new Scanner (System.in);
		do {
			System.out.println("Introduce un numero:");
			numero = input.nextInt();
			guardado = guardado + numero;
			contador++;
		}while(numero != 0);
		System.out.println("Suma de numeros introducidos: " + guardado);
		System.out.println("Cantidad de numeros introducidos: " + contador);
		input.close();

	}

}
