package ejercicio22;

import java.util.Scanner;

public class Ejercicio22 {

	public static void main(String[] args) {
/*Realizar un juego para adivinar un n�mero. Para ello se genera un n�mero aleatorio de 1 al
100 y luego se va pidiendo n�meros indicando �mayor� o �menor� seg�n sea mayor o menor
con respecto al n�mero aleatorio generado. El proceso termina cuando el usuario acierta.*/
		Scanner input = new Scanner (System.in);
		
		int random = (int) (Math.random() * 100);
		System.out.println("Introduce un numero");
		int numero = input.nextInt();
		
		do {
			if(random > numero) {
				System.out.println("Mayor");
				numero = input.nextInt();
			}else if(random < numero) {
				System.out.println("Menor");
				numero = input.nextInt();
			}
		}while(random != numero);
			System.out.println("Correcto");

		input.close();
	}

}
