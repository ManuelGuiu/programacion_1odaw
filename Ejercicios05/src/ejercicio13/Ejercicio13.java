package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		/*) Programa que permite calcular la potencia de un n�mero. Pedir� por teclado la base y el
exponente y mostrar� el resultado de la potencia. El exponente es un entero mayor o igual
que 0. La base es un n�mero entero positivo o negativo. Sin usar la clase Math*/

		Scanner input = new Scanner (System.in);
		
		System.out.println("Para calcular la potencia, introduce base:");
		int base = input.nextInt();
		System.out.println("Introduce el exponente:");
		int exponente = input.nextInt();
		int resultado = 1;
		
		if(exponente >= 0) {
			if(exponente == 0) {
				System.out.println("1");
			}else {
				
				for(int i = 1; i <= exponente; i++) {
					
					resultado = resultado * base;
				}
				System.out.println(resultado);
			}
		}
		
		input.close();
	}

}
