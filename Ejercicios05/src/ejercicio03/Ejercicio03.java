package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {
	static final String FIN = "Fin del programa";
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner (System.in);
		System.out.println("Introduce un numero mayor a 1");
		int num = input.nextInt();
		int num2 = num;
		
		do {
			System.out.println(num);
			num --;
			
		}while ( num >= 1);
		
		do {
			System.out.print(num2 + " ");
			num2 --;
			
		}while ( num2 >= 1);
		System.out.println();
		System.out.println(FIN);
		input.close();
	}

}
