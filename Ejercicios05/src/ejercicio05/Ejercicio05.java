package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {
	static final String FIN = "Fin del programa";
	
	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		System.out.println("Introuce un inicio");
		int inicio = input.nextInt();
		System.out.println("Introduce un final");
		int fin = input.nextInt();
		System.out.println("Introduce un salto");
		int salto = input.nextInt();
		
		
		if ( inicio > fin ) {
			if ( salto > 0 ) {
				for(int i = inicio; (i - salto) >= fin;) {
					i = i - salto;
					System.out.println(i);
				}
				for(int i = inicio; (i - salto) >= fin;) {
					i = i - salto;
					System.out.print(i + " ");
				}
				
				System.out.println("\n" + FIN);
				
			}else {
				System.out.println("el sal to no puede ser negativo");
				System.out.println(FIN);
			}
			
			
		}else if (inicio < fin ) {
			if ( salto > 0 ) {
				for(int i = fin; (i - salto) >= inicio;) {
					i = i - salto;
					System.out.println(i);
				}
				for(int i = fin; (i - salto) >= inicio;) {
					i = i - salto;
					System.out.print(i + " ");
				}
				
				System.out.println("\n" + FIN);
				
			}else {
				System.out.println("el sal to no puede ser negativo");
				System.out.println(FIN);
			}
			
			
		}else {
			System.out.println("No hay un numero mayor que otro");
			System.out.println(FIN);
		}
		
		input.close();
		
	}

}
