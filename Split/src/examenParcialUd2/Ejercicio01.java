package examenParcialUd2;

import java.util.Scanner;

public class Ejercicio01 {
static Scanner input = new Scanner(System.in);
	public static void main(String[] args) {
		String[] frases = rellenarVector();
		visualizarPalabrasLetras(frases);
	}
	
	public static String[] rellenarVector() {
		String[] frases = new String[3];
		for(int i = 0; i < frases.length; i++) {
			System.out.println("Introduce el valor del vector: " + i);
			frases[i] = input.nextLine();
		}
		return frases;
	}
	
	public static void visualizarPalabrasLetras(String[] frases) {
		System.out.println("Introduce la componente:");
		int componente = input.nextInt();
		System.out.println("La frase de la componente es " + frases[componente]);
		int cantidadEspacios = 0;
		for(int i = 0; i < frases[componente].length(); i++) {
			if(frases[componente].charAt(i) == ' ') {
				cantidadEspacios++;
			}
		}
		
		System.out.println("Sus palabras y letras son:");
		for(int i = 0; i < cantidadEspacios; i++) {
			String[] partes = frases[componente].split(" ");
			System.out.println(partes[i] + ", " + partes[i].length());
		}
	}
}
