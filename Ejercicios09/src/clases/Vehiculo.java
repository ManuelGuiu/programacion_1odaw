package clases;

public class Vehiculo {

	private String tipo;
	private String marca;
	private float consumo;
	private float kmTotales;
	private int numRuedas;
	private boolean funciona;

	public Vehiculo(String tipo, String marca) {
		this.tipo = tipo;
		this.marca = marca;
		kmTotales = 0;
		funciona = true;
	}

	public Vehiculo(String tipo, String marca, float consumo, int numRuedas) {
		this.tipo = tipo;
		this.marca = marca;
		this.consumo = consumo;
		this.numRuedas = numRuedas;
		kmTotales = 0;
		funciona = true;
	}

	public Vehiculo() {
		this.tipo = "Bolido";
		this.marca = "Dodge";
		this.consumo = 5;
		this.kmTotales = 10;
		this.numRuedas = 5;
		this.funciona = true;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public float getConsumo() {
		return consumo;
	}

	public void setConsumo(float consumo) {
		this.consumo = consumo;
	}

	public float getKmTotales() {
		return kmTotales;
	}

	public int getNumRuedas() {
		return numRuedas;
	}

	public void setNumRuedas(int numRuedas) {
		this.numRuedas = numRuedas;
	}

	public boolean isFunciona() {
		return funciona;
	}

	public void setFunciona(boolean funciona) {
		this.funciona = funciona;
	}

	public float combustibleConsumido() {
		float consumido = this.consumo * this.kmTotales;
		return consumido;
	}

	public float combustibleConsumido(float kmTotales) {
		float consumido = (consumo * kmTotales) / 100;
		return consumido;
	}

	public float trucarCuentaKm() {
		kmTotales = 0;
		return kmTotales;
	}

	public float trucarCuentaKm(float kmTotales) {
		this.kmTotales = kmTotales;
		return this.kmTotales;
	}
}
