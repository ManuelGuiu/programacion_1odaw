package principal;

import clases.Vehiculo;

public class Principal {

	public static void main(String[] args) {
		Vehiculo vh1 = new Vehiculo("Berlina", "Renault");
		System.out.println("Datos del vehiculo:");
		System.out.println(vh1.toString());
	}

}
