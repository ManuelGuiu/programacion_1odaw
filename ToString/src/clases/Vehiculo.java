package clases;

public class Vehiculo {
	private String tipo;
	private String marca;
	
	public Vehiculo(String tipo, String marca) {
		this.tipo = tipo;
		this.marca = marca;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public String toString() {
		return "Tipo de coche: " + this.tipo + " Marca del coche " + this.marca;
	}
}
