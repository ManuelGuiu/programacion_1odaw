package ejercicio13;

import java.util.Scanner;

public class matrices13 {

	public static void main(String[] args) {
		int[][] matriz = new int[10][10];
		Scanner input = new Scanner(System.in);
		
		//introducir los datos de la matriz
		for(int i = 0; i < matriz.length; i++) {
			for(int j = 0; j < matriz[i].length; j++) {
				matriz[i][j] = (int) Math.round(Math.random()*50);
			}
		}
		
		System.out.println("Dame un numero");
		int num = input.nextInt();
		
		buscarNumero(matriz,num);
		
		input.close();
	}
	
	public static void buscarNumero(int[][] matriz, int numero) {
		int contador = 0;
		for(int i = 0; i < matriz.length; i++) {
			for(int j = 0; j < matriz[i].length; j++) {
				if(matriz[i][j] == numero) {
					contador++;
					System.out.println("(" + i + "," + j + ")");
				}
			}
		}
		System.out.println("El numero " + numero + " ha aparecido " + contador + " veces");
	}

}
