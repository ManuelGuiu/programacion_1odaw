package ejercicio1;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		
		System.out.println("Elige una de las tres opciones:");
		System.out.println("1 - comprobar n�mero");
		System.out.println("2 - comprobar caracter");
		System.out.println("3 - salir");
		int menu = input.nextInt();
		
		switch (menu) {
			case 1:
				System.out.println("Introduce un n�mero entero:");
				int entero = input.nextInt();
				
				if (entero<0) {
					System.out.println("N�mero negativo");
				}else if (entero%2!=0) {
					System.out.println("N�mero positivo e impar");
				}else if (entero%2==0) {
					System.out.println("N�mero positivo y par");
				}
				break;
				
			case 2:
				input.nextLine();
				System.out.println("Introduce una cadena:");
				String cadena = input.nextLine();
				if (cadena.length()>1) {
					System.out.println("Contiene m�s de un caracter");
				}else if (cadena.charAt(0)>='a' && cadena.charAt(0)<='z') {
					System.out.println("Caracter en min�scula");
				}else if (cadena.charAt(0)>='A' && cadena.charAt(0)<='Z') {
					System.out.println("Caracter en may�scula");
				}else if (cadena.charAt(0)>='0' && cadena.charAt(0)<='9') {
					System.out.println("Cifra num�rica");
				}
				break;
				
			case 3:
				System.out.println("Fin del programa");
				break;
				
			default:
				System.out.println("Valor introducido incorrecto");
		}
		
		input.close();
	}

}
