package ejercicio2;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		
		System.out.println("Introduce un correo electr�nico con el siguiente formato 'xxxxxxx@xxxx");
		String correo = input.nextLine();
		
		if (correo.contains("@")) {
			if (correo.indexOf('@') != correo.lastIndexOf('@')) {
				System.out.println("Contiene m�s de un @");
			}else {
				System.out.println("Nombre del email " + correo.substring(0,correo.indexOf('@')));
				System.out.println("Dominio del email " + correo.substring(correo.indexOf('@') + 1));
			}
		}else {
			System.out.println("No es un correo.");
			System.out.println("Fin del programa.");
		}
		
		input.close();

	}

}
