package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		
		System.out.println("Introduce una cadena de tres carácteres:");
		String cadena = input.nextLine();
		
		
		if (cadena.length() != 3) {
			System.out.println("La cadena no tiene tres carácteres.");
			System.out.println("Fin del programa.");
			
		}else if ((cadena.charAt(0) >= '0' && cadena.charAt(0) <= '9') && (cadena.charAt(1) >= '0' && cadena.charAt(1) <= '9') && (cadena.charAt(2) >= '0' && cadena.charAt(2) <= '9')){
			int primero = Integer.parseInt(cadena.substring(0,1));
			int segundo = Integer.parseInt(cadena.substring(1,2));
			int tercero = Integer.parseInt(cadena.substring(2));
			System.out.println(primero + segundo + tercero);
			
		}else {
			System.out.println("No todos son cifras");
		}
		
		input.close();

	}

}
