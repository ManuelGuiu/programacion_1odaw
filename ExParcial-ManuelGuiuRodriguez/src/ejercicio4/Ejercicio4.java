package ejercicio4;

import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		
		System.out.println("Introduce una cadena de texto:");
		String cadena = input.nextLine();
		System.out.println("Introduce un caracter:");
		String caracter = input.nextLine();
		
		if (cadena.contains(caracter)) {
			System.out.println(cadena.substring(cadena.indexOf(caracter)));
		}else {
			System.out.println("La cadena no contiene dicho caracter.");
		}
		
		input.close();
		
	}

}
