package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int numero = 0;
		double media = 0;
		int numeroPar = 0;
		int numeroImpar = 0;
		int contador = 0;
		
		do {
			
			System.out.println("Introduce un numero");
			numero = input.nextInt();
			
			if(numero >= 0) {
				if(numero % 2 == 0) {
					System.out.println("El numero es par");
					numeroPar++;
				}else {
					System.out.println("El numero es impar");
					numeroImpar++;
				}
				
				media = media + numero;
				contador++;
			}
		}while(numero >= 0);
		
		media = media / contador;
		System.out.println("La media es " + media);
		
		if(numeroPar > numeroImpar) {
			System.out.println("Mas pares");
		}else if(numeroPar < numeroImpar){
			System.out.println("Mas impares");
		}else {
			System.out.println("Misma cantidad de pares e impares");
		}
		
		input.close();

	}

}
