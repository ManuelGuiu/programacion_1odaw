package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int menu = 0;
		
		do {
			System.out.println("Selecciona una de las siguientes opciones:");
			System.out.println("1 - contar caracteres");
			System.out.println("2 - contrase�a correcta");
			System.out.println("3 - salir");
			menu = input.nextInt();
			input.nextLine();
			
			switch(menu) {
			case 1:
				System.out.println("Introduce un caracter");
				String caracterCadena = input.nextLine();
				char caracter = caracterCadena.toLowerCase().charAt(0);
				System.out.println("Introduce una cadena");
				String cadena = input.nextLine();
				int contador = 0;
				
				for (int i = 0; i < cadena.length(); i++) {
					if (cadena.toLowerCase().charAt(i) == caracter) {
						contador++;
					}	
				}
				System.out.println("El carcater aparece " + contador + " veces en la cadena");
				break;
			case 2:
				System.out.println("Introduce una contrase�a");
				String pass = input.nextLine();
				boolean vocal;
				int contadorNumeros = 0;
				int contadorCaracter = 0;
				
				if(pass.toLowerCase().contains("a") || pass.toLowerCase().contains("e") || pass.toLowerCase().contains("i") || pass.toLowerCase().contains("o") || pass.toLowerCase().contains("u")) {
					vocal = true;
				}else {
					vocal = false;
				}
				
				for (int i = 0; i < pass.length(); i++) {
					if (pass.charAt(i) >= '0' && pass.charAt(i) <= '9') {
						contadorNumeros++;
					}
				}
				
				for (int j = 0; j < pass.length(); j++) {
					if(pass.charAt(j) >= 'a' && pass.charAt(j) <= 'z') {
						contadorCaracter++;
					}
					if(pass.charAt(j) == '�') {
						contadorCaracter++;
					}
				}
				if(contadorNumeros == 0 && vocal && contadorCaracter == pass.length()) {
					System.out.println("Contrase�a correcta");
				}else {
					System.out.println("Contrase�a incorrecta");
				}
				break;
			case 3:
				System.out.println("Fin de programa");
				break;
			default:
				System.out.println("Introduce una de las 3 opciones");
			}
		}while(menu != 3);
		
		
		input.close();

	}

}
