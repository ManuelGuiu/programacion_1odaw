package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String cadena = "";
		int contadorMayus = 0;
		int contadorCadenasLargas = 0;
		String cadenaLarga = "";
		String cadenaCorta = "estosoloesrellenoparalacadenasinimportancia";
		
		do {
			System.out.println("Introduce una cadena");
			cadena = input.nextLine();
			
			if(!cadena.equals("fin")) {
				for(int i = 0; i < cadena.length(); i++) {
					if(cadena.charAt(i) >= 'A' && cadena.charAt(i) <= 'Z') {
						contadorMayus++;
					}
					if(cadena.charAt(i) == '�') {
						contadorMayus++;
					}
				}
				
				if(cadena.length() > 5) {
					contadorCadenasLargas++;
				}
				
				if(cadena.length() > cadenaLarga.length()) {
					cadenaLarga = cadena;
				}
				
				if(cadena.length() < cadenaCorta.length()) {
					cadenaCorta = cadena;
				}
				
			}
		}while(!cadena.equals("fin"));
		
		System.out.println("La cantidad total de mayusculas es " + contadorMayus);
		System.out.println("Numero de cadenas con mas de 5 caracteres " + contadorCadenasLargas);
		System.out.println("La cadena mas larga es " + "'" + cadenaLarga + "'");
		System.out.println("La cadena mas corta es " + "'" + cadenaCorta + "'");
		
		
		input.close();

	}

}
