package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		
		System.out.println("Introduce una cadena de fin:");
		String fin = input.nextLine();
		String pedida = "";
		int contadorLongitud = 0;
		int contadorMayus = 0;
		int contadorCadenasMayus = 0;
		String cadenaLarga = "";
		
		do {
			contadorMayus = 0;
			System.out.println("Introduce una cadena");
			pedida = input.nextLine();
			if(pedida.length() >= 5) {
				contadorLongitud++;
			}
			for(int i = 0; i < pedida.length(); i++) {
				if(pedida.charAt(i) >= 'A' && pedida.charAt(i) <= 'Z') {
					contadorMayus++;
				}
			}
			if(contadorMayus > 0) {
				contadorCadenasMayus++;
			}
			if (pedida.length() > cadenaLarga.length()) {
				cadenaLarga = pedida;
			}
		}while(!fin.equals(pedida));
		
		System.out.println("Numero de cadenas con mas de 5 caracteres " + contadorLongitud);
		System.out.println("Numero de cadenas con mayusculas " + (contadorCadenasMayus));
		System.out.println("Cadena m�s larga " + cadenaLarga);
		
		input.close();

	}

}
