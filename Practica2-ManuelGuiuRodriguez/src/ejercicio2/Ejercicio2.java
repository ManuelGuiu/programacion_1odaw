package ejercicio2;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		
		System.out.println("Introduce un numero positivo:");
		int numPos = input.nextInt();
		input.nextLine();
		int numero = 0;
		float suma = 0;
		
		if (numPos > 0) {
			for (int i = 0; i < numPos; i++) {
				System.out.println("Introduce el numero " + (i + 1));
				numero = input.nextInt();
				suma = suma + numero;
				if (numero % 2 == 0) {
					System.out.println("Numero par");
				}
			}
		System.out.println("Media " + suma / numPos);
		
		}else if(numPos == 0) {
			
			System.out.println(0);
			
		}else {
			
			System.out.println("Introduce un numero positivo.");
			
		}
		input.close();

	}

}
