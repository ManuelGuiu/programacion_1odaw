package ejercicio4;

import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		
		String pedida = "";
		int numero = 0;
		int suma = 0;
		int contador = 0;
		int numeroGrande1 = 0;
		int numeroGrande2 = 0;
		int pase = 0;
		
		do {
			System.out.println("Introduce un numero o 'fin'.");
			pedida = input.nextLine();
			if(!pedida.equals("fin")) {
				numero = Integer.parseInt(pedida);
				suma = suma + numero;
				contador++;
				
				if(numero > numeroGrande1) {
					numeroGrande1 = numero;
					pase = numero;
					if(numeroGrande1 > numeroGrande2) {
						pase = numeroGrande2;
						numeroGrande2 = numeroGrande1;
						numeroGrande1 = pase;
					}
				}
			}
		}while(!pedida.equals("fin"));
		
		System.out.println("La suma de los numeros es " + suma);
		if(contador == 1) {
			System.out.println("Unico numero " + numero);
		}
		if(contador >= 2) {
			System.out.println("Numero mas grande " + numeroGrande2);
			System.out.println("Segundo numero mas grande " + numeroGrande1);
		}
		
		input.close();

	}

}
