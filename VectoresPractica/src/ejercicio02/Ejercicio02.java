package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {
	/*
	 * 2)Crea un array de n�meros donde le indicamos por teclado el tama�o del
	 * array, rellenaremos el array con n�meros aleatorios entre 0 y 9, al final
	 * muestra por pantalla el valor de cada posici�n y la suma de todos los
	 * valores. Haz un m�todo para rellenar el array (que tenga como par�metros los
	 * n�meros entre los que tenga que generar), para mostrar el contenido y la suma
	 * del array y un m�todo privado para generar n�mero aleatorio (lo puedes usar
	 * para otros ejercicios).
	 */

	public static void main(String[] args) {
		int vectorMain[]= array();
		mostrar(vectorMain);
		suma(vectorMain);
	}
	
	static int aleatorio() {
		int numero = (int) Math.round(Math.random()*10);
		return numero;
	}
	
	static int[] array(){
		Scanner input = new Scanner(System.in);
		System.out.println("Indica el tama�o del vector.");
		int longitud = input.nextInt();
		int vector[] = new int[longitud];
		for(int i = 0; i < vector.length; i++) {
			int aleatorio = aleatorio();
			if(aleatorio >= 0 && aleatorio <= 9) {
				vector[i] = aleatorio;
			}
		}
		input.close();
		return vector;
	}
	
	static int suma(int[] vector) {
		int suma = 0;
		for(int i = 0; i < vector.length; i++) {
			suma = suma + vector[i];
		}
		System.out.println("La suma de los valores es " + suma);
		return suma;
	}
	
	static void mostrar(int[] vector) {
		for(int i = 0; i < vector.length; i++) {
			System.out.println("En la posicion " + i + " el valor del vector es: " + vector[i]);
		}
	}

}
