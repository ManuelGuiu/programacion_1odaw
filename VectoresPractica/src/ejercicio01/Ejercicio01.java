package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {
	/*
	 * 1)Crea un array de 10 posiciones de n�meros con valores pedidos por teclado.
	 * Muestra por consola el �ndice y el valor al que corresponde. Haz dos m�todos,
	 * uno para rellenar valores y otro para mostrar.
	 */

	public static void main(String[] args) {
		int vectorVoid[] = lector();
		mostrar(vectorVoid);
	}
	
	static int[] lector() {
		Scanner input = new Scanner(System.in);
		int[] vector = new int[10];
		for (int i = 0; i < 10; i++) {
			System.out.println("Introduce el valor del vector " + (i + 1));
			vector[i] = input.nextInt();
		}
		input.close();
		return vector;
	}
	
	static void mostrar(int vector[]) {
		for(int i = 0; i < 10; i++) {
			System.out.println("Posicion del vector " + i + " valor del vector " + vector[i]);
		}
	}

}
