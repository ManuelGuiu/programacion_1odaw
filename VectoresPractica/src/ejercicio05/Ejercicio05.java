package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {
	
	/*
	 * 5) Crea un array de caracteres que contenga de la �A� a la �Z� (solo las
	 * may�sculas). Despu�s, ve pidiendo posiciones del array por teclado y si la
	 * posici�n es correcta, se a�adir� a una cadena que se mostrara al final, se
	 * dejar� de insertar cuando se introduzca un -1. Por ejemplo, si escribo los
	 * siguientes n�meros:
	 *  0 //A�adir� la �A� 
	 *  5 //A�adir� la �F� 
	 *  25 //A�adir� la �Z�
	 *  50 //Error, inserte otro n�mero 
	 *  -1 //fin 
	 *  Cadena resultante: AFZ
	 */

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		char caracter[] = new char[26];
		char c = 'A';
		
		for(int i = 0; i <= 25; i++) {
			caracter[i] = c;
			c++;
		}
		
		int posicion;
		String cadena = "";
		
		do {
			System.out.println("Posicion del array:");
			posicion = input.nextInt();
			input.nextLine();
			
			if(posicion >= 0 && posicion <= 26) {
				System.out.println(caracter[posicion]);
				cadena = cadena + caracter[posicion];
			}
			
		}while(posicion != -1);
		
		System.out.println(cadena);
		
		input.close();
	}

}
