package ejercicio07;

import java.util.Scanner;

public class Ejercicio07 {

	/*
	 * 7)Crea dos arrays de n�meros con una posici�n pasado por teclado.Uno de ellos
	 * estar� rellenado con n�meros aleatorios y el otro apuntara al array anterior,
	 * despu�s crea un nuevo array con el primer array (usa de nuevo new con el
	 * primer array) con el mismo tama�o que se ha pasado por teclado, rell�nalo de
	 * nuevo con n�meros aleatorios.Despu�s, crea un m�todo que tenga como
	 * par�metros, los dos arrays y devuelva uno nuevo con la multiplicaci�n de la
	 * posici�n 0 del array1 con el del array2 y as� sucesivamente. Por �ltimo,
	 * muestra el contenido de cada array.
	 */

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.println("Introduce la longitud de los vectores.");
		int longitud = input.nextInt();
		input.nextLine();
		int array1[] = new int[longitud];
		for (int i = 0; i < array1.length; i++) {
			array1[i] = (int) Math.round(Math.random() * 100);
		}

		int array2[] = array1;

		array1 = new int[longitud];

		for (int i = 0; i < array1.length; i++) {
			array1[i] = (int) Math.round(Math.random() * 100);
		}

		multiplicar(array1, array2);

		input.close();

	}

	static int[] multiplicar(int array1[], int array2[]) {

		int multiplicar[] = new int[array1.length];
		for (int i = 0; i < multiplicar.length; i++) {
			multiplicar[i] = array1[i] * array2[i];
		}

		System.out.println("Los valores del array3 son:");
		for (int i = 0; i < multiplicar.length; i++) {
			System.out.println(multiplicar[i]);
		}

		return multiplicar;
	}

}