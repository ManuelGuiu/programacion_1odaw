package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	/*
	 * 6) Pide al usuario por teclado una frase y pasa sus caracteres a un array de
	 * caracteres. Puedes hacer con o sin m�todos de String.
	 */

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce una cadena:");
		String cadena = input.nextLine();
		
		char caracteres[] = new char[cadena.length()];
		
		if(cadena.length() == 0) {
			System.out.println("La cadena esta vacia.");
		}else {
			for(int i = 0; i < cadena.length(); i++) {
				caracteres[i] = cadena.charAt(i);
				if(caracteres[i] == ' ') {
					System.out.println("espacio");
				}else {
					System.out.println(caracteres[i]);
				}
				
			}
		}

		input.close();
	}

}
