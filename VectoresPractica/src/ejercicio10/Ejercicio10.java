package ejercicio10;

public class Ejercicio10 {

	/*
	 * 10) Crea un array de n�meros y otro de String de 10 posiciones donde
	 * insertaremos notas entre 0 y 10 (debemos controlar que inserte una nota
	 * valida), pudiendo ser decimal la nota en el array de n�meros, en el de
	 * Strings se insertaran los nombres de los alumnos. Despu�s, crearemos un array
	 * de String donde insertaremos el resultado de la nota con palabras. 
	 * Si la nota esta entre 0 y 4,99, ser� un suspenso Si esta entre 5 y 6,99, ser� un bien.
	 * Si esta entre 7 y 8,99 ser� un notable.
	 * Si esta entre 9 y 10 ser� un sobresaliente. Muestra por pantalla, el alumno su nota 
	 * y su resultado en palabras. 
	 * Crea los m�todos que creas conveniente.
	 */

	public static void main(String[] args) {
		String nombres[] = {"Michelle Gamble","Yvette Cruz","Craig Steele","Ori Daniel","Germaine Salinas","Caldwell Alford","Shelley Reyes","Shaine Short","Laith Harrison","Jaime Howard"};
		
		
		double notas[] = new double[10];
		for(int i = 0; i < 10; i++) {
			notas[i] = notaRandom();
		}
		
		String calificacion[] = new String[10];
		for (int i = 0; i < 10; i++) {
			if(notas[i] >= 0.00 && notas[i] <= 4.99) {
				calificacion[i] = "Suspenso";
			}else if(notas[i] >= 5.00 && notas[i] <= 6.99) {
				calificacion[i] = "Bien";
			}else if(notas[i] >= 7.00 && notas[i] <= 8.99) {
				calificacion[i] = "Notable";
			}else if(notas[i] >= 9.00 && notas[i] <= 10.00) {
				calificacion[i] = "Sobresaliente";
			}
		}
		
		for (int i = 0; i < 10; i++) {
			System.out.println(nombres[i] + " " + notas[i] + " " + calificacion[i]);
		}
		
	}
	
	static double notaRandom() {
		double nota = Math.round(Math.random() * 1000) / 100.00;
		return nota;
	}

}
