package ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {

	/*
	 * 11) Crea una aplicaci�n que pida un numero por teclado y despu�s
	 * comprobaremos si el numero introducido es capic�a, es decir, que se lee igual
	 * sin importar la direcci�n. Por ejemplo, si introducimos 30303 es capic�a, si
	 * introducimos 30430 no es capic�a. Piensa como puedes dar la vuelta al n�mero.
	 * Una forma de pasar un n�mero a un array es esta
	 * Character.getNumericValue(cadena.charAt(posicion)).
	 */

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un numero que sea capicua");
		String cadenaNum = input.nextLine();

		char v1[] = new char[cadenaNum.length()];
		char v2[] = new char[cadenaNum.length()];

		for (int i = 0; i < v1.length; i++) {
			v1[i] = cadenaNum.charAt(i);
		}

		for (int i = 0; i < v2.length; i++) {
			v2[i] = cadenaNum.charAt(i);
		}

		int contadorPar = 0;
		int contadorIn = 0;
		int parada = 0;
		boolean comprobador = false;
		boolean basura = false;
		
		if(basura) {
			System.out.println(contadorIn);
		}

		for (int i = 0, j = v1.length - 1; i < v1.length; i++, j--) {
			if (v1.length % 2 == 0) {
				if (v1[i] == v2[j]) {
					contadorPar++;
				}
			} else {
				if ((v1.length / 2) + 1 != parada) {
					if (v1[i] == v2[j]) {
						contadorIn++;
					}
					parada++;
					if (parada == v1.length / 2) {
						comprobador = true;
					}
				}
			}
		}

		if (contadorPar == v1.length || comprobador) {
			System.out.println("Es capicua");
		} else {
			System.out.println("No es capicua");
		}

		input.close();
	}

}
