package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {
	/*
	 * 3)Crea un array de n�meros de un tama�o pasado por teclado, el array
	 * contendr� n�meros aleatorios primos entre los n�meros deseados, por �ltimo,
	 * nos indica cual es el mayor de todos.Haz un m�todo para comprobar que el
	 * n�mero aleatorio es primo, puedes hacer todos los m�todos que necesites.
	 */

	public static void main(String[] args) {
		int[] arrayMain = array();
		comprobar(arrayMain);
		mayor(arrayMain);
	}

	static int[] array() {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce la longitud del vector");
		int longitud = input.nextInt();
		int[] vector = new int[longitud];
		input.close();
		return vector;
	}

	static int aleatorio() {
		int random = (int) Math.round(Math.random() * 100);
		return random;
	}

	static int[] comprobar(int[] vector) {
		int num = 0;
		for (int i = 0; i < vector.length; i++) {
			num = aleatorio();
			if (num % 2 != 0 && num / 2 > 2) {
				if (num % 3 != 0 && num / 3 > 3) {
					if (num % 5 != 0 && num / 5 > 5) {
						if (num % 7 != 0 && num / 7 > 7) {
							if (num % 11 != 0 && num / 11 > 11) {
							} else if ((num % 11 != 0 || num == 11) && num / 11 <= 11) {
								vector[i] = num;
							}else {
								i--;
							}
						} else if ((num % 7 != 0 || num == 7) && num / 7 <= 7) {
							vector[i] = num;
						}else {
							i--;
						}
					} else if ((num % 5 != 0 || num == 5) && num / 5 <= 5) {
						vector[i] = num;
					}else {
						i--;
					}
				} else if ((num % 3 != 0 || num == 3) && num / 3 <= 3) {
					vector[i] = num;
				}else {
					i--;
				}
			} else if ((num % 2 != 0 || num == 2) && num / 2 <= 2) {
				vector[i] = num;
			}else {
				i--;
			}
		}
		for(int i = 0; i < vector.length; i++) {
			System.out.println(vector[i]);
		}
		return vector;
	}
	
	static int mayor(int[] vector) {
		int mayor = 0;
		for(int i = 0; i < vector.length; i++) {
			if(vector[i] > mayor) {
				mayor = vector[i];
			}
		}
		System.out.println("El primitivo mayor es " + mayor);
		return 0;
	}

}