package ejercicio08;

import java.util.Scanner;

public class Ejercicio08 {

	/*
	 * 8)Crea un array de n�meros de un tama�o pasado por teclado, el array
	 * contendr� n�meros aleatorios entre 1 y 300 y mostrar aquellos n�meros que
	 * acaben en un d�gito que nosotros le indiquemos por teclado (debes controlar
	 * que se introduce un numero correcto), estos deben guardarse en un nuevo
	 * array.Por ejemplo, en un array de 10posiciones e indicamos mostrar los
	 * n�meros acabados en 5, podr�a salir 155, 25, etc.
	 */

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce la longitud del vector:");
		int longitud = input.nextInt();
		input.nextLine();

		// creacion del vector y rellnado con numeros aleatorios de 1 a 300
		String v[] = new String[longitud];
		int aleatorio;

		for (int i = 0; i < v.length; i++) {
			aleatorio = (int) Math.round(Math.random() * 1000);
			if (aleatorio >= 1 && aleatorio <= 300) {
				v[i] = String.valueOf(aleatorio);
				System.out.println(v[i]);
			} else {
				i--;
			}
		}

		// peticion y comprobacion de numero por el que acaba la cifra que buscamos
		boolean comprobador = true;
		String ultimoNumCad;
		char ultimoNum;

		do {
			System.out.println("Numero por el que acaba la cifra que buscas");
			ultimoNumCad = input.nextLine();

			if (ultimoNumCad.length() > 1 || ultimoNumCad.length() < 1) {
				System.out.println("Introduce solo un numero.");
				System.out.println("-------------------------");
			} else {
				comprobador = false;
			}
		} while (comprobador);

		ultimoNum = ultimoNumCad.charAt(0);
		int contador = 0;

		// bucle para definir el tama�o del v2 en funcion de numeros encontrados
		for (int i = 0; i < v.length; i++) {
			if (v[i].charAt(v[i].length() - 1) == ultimoNum) {
				contador++;
			}
		}

		// creacion del v2 y rellenado, el contador es para poder trabajar con dos
		// posiciones de vector al mismo tiempo
		String v2[] = new String[contador];
		int contador2 = 0;
		for (int i = 0; i < v.length; i++) {
			if (v[i].charAt(v[i].length() - 1) == ultimoNum) {
				v2[contador2] = v[i];
				System.out.println(v2[contador2]);
				contador2++;
			}
		}

		input.close();
	}

}