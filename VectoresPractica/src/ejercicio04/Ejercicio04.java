package ejercicio04;

public class Ejercicio04 {
	
	/*
	 * 4) Crea un array de n�meros de 100 posiciones, que contendr� los n�meros del
	 * 1 al 100. Obt�n la suma de todos ellos y la media.
	 */

	public static void main(String[] args) {
		
		int array[] = new int[100];
		float suma = 0;

		for (int i = 1; i <= 100; i++) {
			array[i - 1] = i;
		}

		for (int i = 0; i < 100; i++) {
			suma = suma + array[i];
		}

		System.out.println("La suma de los 100 numeros es: " + suma);
		System.out.println("Y su media es: " + suma / array.length);

	}

}
