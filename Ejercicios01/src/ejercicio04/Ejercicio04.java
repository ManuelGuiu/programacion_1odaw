package ejercicio04;

public class Ejercicio04 {

	public static void main(String[] args) {
		int variableA = 2;
		int variableB = 4;
		int variableC = 6;
		int variableD = 8;
		
		//usando otra variable auxiliar
		int auxiliar;
		auxiliar=variableA;
		variableA=variableB;
		variableB=variableC;
		variableC=variableD;
		variableD=auxiliar;
		
		//variableA=variableA+variableC;
		//variableA=variableA-variableB;
		//variableB=variableB+variableD;
		//variableB=variableB-variableC;
		//variableC=variableC+variableD;
		//variableC=variableC-variableB;
		//variableD=variableD-variableB;
		
		System.out.println(variableA);
		System.out.println(variableB);
		System.out.println(variableC);
		System.out.println(variableD);
		

	}

}
