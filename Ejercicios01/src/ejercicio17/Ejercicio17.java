package ejercicio17;

public class Ejercicio17 {

	public static void main(String[] args) {
		//String cadena1="cadena de texto";
			//int valor1=cadena1.length();
		//String cadena2="cadena de texto";
			//int valor2=cadena2.length();
			
		//System.out.println(valor1>valor2?"cadena1 es mayor que cadena2":valor1<valor2?"cadena1 es menor que cadena2":valor1==valor2?"cadena1 es igual a cadena2":" ");
		
		//metodo comprimido
		//System.out.println(cadena1.length()>cadena2.length()?"cadena1 es mayor que cadena2":cadena1.length()<cadena2.length()?"cadena1 es menor que cadena2":cadena1.length()==cadena2.length()?"cadena1 es igual a cadena2":" ");
		
		String cadena1="nueva cadena";
		String cadena2="otracadena";
		
		//compara los valores de la tabla asci
		int resultado = cadena1.compareTo(cadena2);
		
		//resultado > 0 -> "cadena1 > cadena2"
		//resultado < 0 -> "cadena1 < cadena2"
		//resultado == 0 -> "cadena1 = cadena2"
		
		System.out.println(resultado > 0?"*"+cadena1+"*"+" es mayor que "+"*"+cadena2+"*":resultado<0?"*"+cadena1+"*"+" es menor que "+"*"+cadena2+"*":resultado==0?"*"+cadena1+"*"+" es igual que "+"*"+cadena2+"*":" ");
	}

}
