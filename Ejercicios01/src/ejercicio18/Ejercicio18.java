package ejercicio18;
import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("numero int");
		int num = input.nextInt();
		System.out.println("numero leido "+num);
		System.out.println("");
		
		System.out.println("numero byte");
		byte numByte = input.nextByte();
		System.out.println("numero leido "+numByte);
		System.out.println("");
		
		System.out.println("numero short");
		short numShort = input.nextShort();
		System.out.println("numero leido "+numShort);
		System.out.println("");
		
		System.out.println("numero long");
		long numLong = input.nextLong();
		System.out.println("numero leido "+numLong);
		System.out.println("");
		
		System.out.println("numero float");
		float numFloat = input.nextFloat();
		System.out.println("numero leido "+numFloat);
		System.out.println("");
		
		System.out.println("numero double");
		double numDouble = input.nextDouble();
		System.out.println("numero leido "+numDouble);
		System.out.println("");
		
		System.out.println("numero boolean");
		boolean numBoolean = input.nextBoolean();
		System.out.println("numero leido "+numBoolean);
		System.out.println("");
		
		System.out.println("numero boolean");
		char numChar = input.nextLine().charAt(0);
		System.out.println("numero leido "+numChar);
		
		
		input.close();
		
	}

}
