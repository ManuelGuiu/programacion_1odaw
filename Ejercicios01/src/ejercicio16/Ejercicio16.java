package ejercicio16;

public class Ejercicio16 {

	public static void main(String[] args) {
		String cadena="123456";
		int valor=cadena.length();
		int numero=Integer.parseInt(cadena);
		
		System.out.println(valor>=5?numero:"la cadena tiene menos de 5 cifras");
		
		//mismo metodo pero en una sola variable
		System.out.println(cadena.length()>=5?Integer.parseInt(cadena):"la cadena tiene menos de 5 cifras");

	}

}
