package ejercicio11;

public class Ejercicio11 {

	public static void main(String[] args) {
		
		long largopositivo = 9223372036854775807L;
		long largonegativo = -9223372036854775808L;
		
		short pequeñopositivo=(short)largopositivo;
		short pequeñonegativo=(short)largonegativo;
		
		System.out.println(pequeñopositivo);
		System.out.println(pequeñonegativo);

	}

}