package ejercicio10;

public class Ejercicio10 {

	public static void main(String[] args) {
		int variableA = 101;
		
		System.out.println(variableA%5==0?"El numero es multiplo de 5":"El numero no es multiplo de 5");
		System.out.println(variableA%10==0?"El numero es multiplo de 10":"El numero no es multiplo de 10");
		System.out.println(variableA>100?"El numero es mayor a 100":variableA==100?"El numero es igual a 100":variableA<100?"El numero es menor a 100":"No se cumplen las condiciones");
		
	}

}
