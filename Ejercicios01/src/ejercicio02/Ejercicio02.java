package ejercicio02;

public class Ejercicio02 {

	public static void main(String[] args) {
		int variable1 = 2147483647;
		byte variable2 = 127;
		short variable3 = -32768;
		long variable4 = 9223372036854775807L;
		float variable5 = 35.8F;
		double variable6 = 87.4;
		char variable7 = '$';
		
		System.out.println(variable1 + variable2);
		System.out.println(variable2 - variable3);
		System.out.println(variable3 * variable1);
		System.out.println(variable4 / variable3);
		System.out.println(variable5 % variable6);
		System.out.println(variable6 - variable5);
		System.out.println((int)variable7 + variable1);
		
		
	}

}
