package ejercicio09;

public class Ejercicio09 {

	public static void main(String[] args) {
		int variable = 3;
		
		System.out.println(variable % 3==0 && variable% 7==0 && variable >0 ? "El numero es multiplo de 3 y de 7 y es positivo" : variable%3==0 && variable>0 ? "El numero es multiplo de 3 y positivo" : variable%7==0 && variable>0 ? "El numero es multiplo de 7 y positivo":"No se cumplen las condiciones");

	}

}
