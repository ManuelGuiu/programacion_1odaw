package ejercicio15;

public class Ejercicio15 {
	
	static final String MENSAJE_INICIO = "Comienza el programa";
	static final String MENSAJE_FIN = "Termina el programa";
	
	public static void main(String[] args) {
		
		String cadena = "esta es una frase que tiene mas de 20 caracteres";
		String cadena2 = cadena.substring(5,17);
		
		System.out.println(MENSAJE_INICIO);
		System.out.println(cadena + " / " + cadena2);
		System.out.println(MENSAJE_FIN);

	}

}
