package ejercicio14;

public class Ejercicio14 {

	public static void main(String[] args) {
		String cadenadecimal="3.14";
		String cadenalarga="4465789";
		String cadenabyte="127";
		String cadenaint="987";
		
		System.out.println(cadenadecimal);
		System.out.println(cadenalarga);
		System.out.println(cadenabyte);
		System.out.println(cadenaint);
		System.out.println(" ");
		
		double decimal = Double.parseDouble(cadenadecimal);
		long larga = Long.parseLong(cadenalarga);
		byte cbyte = Byte.parseByte(cadenabyte);
		int cint = Integer.parseInt(cadenaint);
		
		System.out.println(decimal);
		System.out.println(larga);
		System.out.println(cbyte);
		System.out.println(cint);

	}

}
