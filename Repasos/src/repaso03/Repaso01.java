package repaso03;

import java.util.Scanner;

public class Repaso01 {

	public static void main(String[] args) {
		//programa que pide un numero 3 veces.
		//indique numero 1 de 3, 2 de 3 y 3 de 3.
		//suma los numero pedidos.
		
		Scanner input = new Scanner(System.in);
		
		int suma = 0;
		
		for (int i = 1; i < 4; i++) {
		System.out.println("Introduce numero " + i + " /3");
		int numero = input.nextInt();
			
			suma = suma + numero;
		}
		
		System.out.println(suma);
		
		input.close();
	}

}
