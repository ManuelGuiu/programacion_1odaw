package repaso03;

import java.util.Scanner;

public class Repaso02 {

	public static void main(String[] args) {
		//programa que pide numeros hasta que se introduce 0 y muestra la suma de los numeros.
		Scanner input = new Scanner(System.in);
		int numero;
		int suma = 0;
		
		do {
			System.out.println("Introduce un numero");
			numero = input.nextInt();
			suma = suma + numero;
			
		}while(numero != 0);
		
		System.out.println(suma);
		input.close();
	}

}