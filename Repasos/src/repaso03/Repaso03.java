package repaso03;

import java.util.Scanner;

public class Repaso03 {

	public static void main(String[] args) {
		//programa que pide una cadena hasta que se introduce FIN � fin.
		Scanner input = new Scanner(System.in);
		String cadena;
		do {
			System.out.println("Introduce una cadena");
			cadena = input.nextLine();
		}while( !(cadena.equals("FIN") || cadena.equals("fin")));
		
		
		input.close();
	}

}
