package repaso05;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String cadena = "";
		
		while(!cadena.equals("0 0")){
			
			System.out.println("Coordenadas:");
			cadena = input.nextLine();
		
			int x = cadena.charAt(0) - 48;
			int y = cadena.charAt(2) - 48;
			
			char coor = 0;
			
			if(x == 1) {
				coor = 'h';
			}else if(x == 2) {
				coor = 'g';
			}else if(x == 3) {
				coor = 'f';
			}else if(x == 4) {
				coor = 'e';
			}else if(x == 5) {
				coor = 'd';
			}else if(x == 6) {
				coor = 'c';
			}else if(x == 7) {
				coor = 'b';
			}else if(x == 8) {
				coor = 'a';
			}
			
			System.out.println(coor + " " + y);
		}
		System.out.println("Fin de programa");
		input.close();

	}

}
