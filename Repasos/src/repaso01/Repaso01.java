package repaso01;

import java.util.Scanner;

public class Repaso01 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		
		System.out.println("1) Sumar enteros");
		System.out.println("2) Calcular el mayor");
		System.out.println("3) Comprobar dni");
		System.out.println("Selecciona una opcion (n�mero):");
		
		int valor=input.nextInt();
		
		if(valor==1) {
			System.out.println("Has selecionado la primera opci�n");
			System.out.println("Introduce un entero");
			int numero1=input.nextInt();
			System.out.println("Introduce otro numero");
			int numero2 =input.nextInt();
			System.out.println(numero1+numero2);
		}else if (valor==2) {
			System.out.println("Has selecionado la segunda opci�n");
			System.out.println("Introduce un decimal");
			double numero1=input.nextDouble();
			System.out.println("Introduce otro numero");
			double numero2 =input.nextDouble();
			System.out.println(numero1>numero2?"Primero es mayor que segundo":numero1<numero2?"Segundo es mayor que primero":"Son iguales");
		}else if(valor==3) {
			//validar un dni 8digitos y una letra mayuscula
			input.nextLine();
			System.out.println("Has selecionado la tercera opci�n");
			System.out.println("Introduce tu dni");
			String cadena=input.nextLine();
			String digito=cadena.substring(0,8);
			char letra=cadena.charAt(8);
			
			if (digito.length()==8&&(letra>=65&&letra<=90)) {
				System.out.println("dni correcto");
			}else {System.out.println("datos dni incorrectos");}
		}else {
			System.out.println("Opci�n incorrecta");
			}
		
		
		input.close();
	}

}
