package paquete02;

public class Coche {
	
	private int nRuedas;
	private double vel;
	private String modelo;
	
	public Coche() {
		this.nRuedas = 5;
		this.vel = 240;
		this.modelo = "cupe";
	}
	
	public Coche(int nRuedas, int vel) {
		this.nRuedas = nRuedas;
		this.vel = vel;
	}
	
	public Coche(int nRuedas, int vel, String modelo) {
		this(nRuedas, vel);
		this.modelo = modelo;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getnRuedas() {
		return nRuedas;
	}

	public void setnRuedas(int nRuedas) {
		this.nRuedas = nRuedas;
	}

	public double getVel() {
		return vel;
	}

	public void setVel(double vel) {
		this.vel = vel;
	}
	
	public void acelerar(int vel) {
		this.vel = this.vel + vel;
	}
	
	public void frenar(int vel) {
		this.vel = this.vel - vel;
	}
	
}
