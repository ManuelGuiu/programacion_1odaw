package paquete02;

public class Principal {

	public static void main(String[] args) {
		Coche hummer = new Coche();
		System.out.println("Numero de ruedas");
		System.out.println(hummer.getnRuedas());
		System.out.println("Velocidad maxima");
		System.out.println(hummer.getVel());
		System.out.println("Cambiamos valores");
		hummer.setnRuedas(4);
		hummer.setVel(200);
		System.out.println("Numero de ruedas");
		System.out.println(hummer.getnRuedas());
		System.out.println("Velocidad maxima");
		System.out.println(hummer.getVel());

		///////////////////////////////////////

		Coche hummer2 = new Coche(8, 300);
		System.out.println("Numero de ruedas");
		System.out.println(hummer2.getnRuedas());
		System.out.println("Velocidad maxima");
		System.out.println(hummer2.getVel());
		System.out.println("Cambiamos valores");
		hummer2.setnRuedas(4);
		hummer2.setVel(200);
		System.out.println("Numero de ruedas");
		System.out.println(hummer2.getnRuedas());
		System.out.println("Velocidad maxima");
		System.out.println(hummer2.getVel());

		///////////////////////////////////////

		//aceleramos
		System.out.println("Aceleramos");
		hummer2.acelerar(10);
		System.out.println(hummer2.getVel());
		//frenamos
		System.out.println("Frenamos");
		hummer2.frenar(30);
		System.out.println(hummer2.getVel());
		
		///////////////////////////////////////
		
		Coche hummer3 = new Coche(6, 120, "cupe");
		System.out.println(hummer3.getModelo());
		System.out.println(hummer3.getnRuedas());
		System.out.println(hummer3.getVel());
		
	}

}
