package paquete01;

public class Persona{
    
    private String nombre;
    private int edad;
    private double altura;
    
	public Persona(){
       this.nombre = "Maricon";
       this.edad = 69;
       this.altura = 1.80;
    }
    
    public Persona(String nombre, int edad){
       this.nombre = nombre;
       this.edad = edad;
    }
    
    public Persona(String nombre, int edad, double altura) {
    	this(nombre,edad);
    	this.altura = altura;
    }

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}
    
   
}

