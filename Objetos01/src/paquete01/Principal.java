package paquete01;

public class Principal {

	public static void main(String[] args) {
		// objeto de la clase persona
		Persona persiana = new Persona();

		// mostramos valores
		System.out.println("Mostramos el nombre");
		System.out.println(persiana.getNombre());
		System.out.println("Mostramos edad");
		System.out.println(persiana.getEdad());

		System.out.println("Cambiamos el nombre");
		persiana.setNombre("K");
		System.out.println("Cambiamos la edad");
		persiana.setEdad(0);
		System.out.println("Mostramos el nombre");
		System.out.println(persiana.getNombre());
		System.out.println("Mostramos edad");
		System.out.println(persiana.getEdad());

		// objeto de la clase persona
		Persona persiana2 = new Persona("Pepito", 1);

		// mostramos valores
		System.out.println("Mostramos el nombre");
		System.out.println(persiana2.getNombre());
		System.out.println("Mostramos edad");
		System.out.println(persiana2.getEdad());

		System.out.println("Cambiamos el nombre");
		persiana2.setNombre("K");
		System.out.println("Cambiamos la edad");
		persiana2.setEdad(0);
		System.out.println("Mostramos el nombre");
		System.out.println(persiana2.getNombre());
		System.out.println("Mostramos edad");
		System.out.println(persiana2.getEdad());
	}
}
