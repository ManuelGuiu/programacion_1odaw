package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Introduce un cadena de texto.");
		String cadena=input.nextLine();
		char caracter=cadena.charAt(0);
		System.out.println(cadena.charAt(0)==caracter?"La cadena empieza por el caracter":"La cadena no empieza por el caracter");		
		input.close();
	}

}
