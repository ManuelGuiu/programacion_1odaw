package ejercicio08;

import java.util.Scanner;

public class Ejercicio08 {

	public static void main(String[] args) {
		Scanner input= new Scanner(System.in);
		System.out.println("Introduce un numero entero de 5 cifras");
		int numero=input.nextInt();
		System.out.println(numero/10000);
		System.out.println(numero/1000);
		System.out.println(numero/100);
		System.out.println(numero/10);
		System.out.println(numero);
		input.nextLine();
		System.out.println("Introduce un numero entero de 5 cifras");
		String cadena=input.nextLine();
		System.out.println(cadena.substring(0,0));
		System.out.println(cadena.substring(0,1));
		System.out.println(cadena.substring(0,2));
		System.out.println(cadena.substring(0,3));
		System.out.println(cadena.substring(0,4));
		System.out.println(cadena.substring(0,5));
		input.close();

	}

}
