package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Introduce una cadena");
		String cadena1=input.nextLine();
		System.out.println("Introduce una segunda cadena");
		String cadena2=input.nextLine();
		System.out.println(cadena1.contains(cadena2)?"La cadena 1 contiene la cadena2":cadena2.contains(cadena1)?"La cadena 2 contiene la cadena1":"Ninguna de las dos cadena contiene a la otra");
		
		input.close();

	}

}
