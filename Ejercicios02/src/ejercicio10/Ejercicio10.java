package ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Introduce tu fecha de nacimiento en el orden indicado y en formato num�rico.");
		System.out.println("Introduce el d�a:");
		int dia=input.nextInt();
		System.out.println("Introduce el mes:");
		int mes=input.nextInt();
		System.out.println("Introduce el a�o:");
		int a�o=input.nextInt();
		int suma=dia+mes+a�o;
		int suerte=(suma/1000)+((suma/100)%10)+((suma/10)%100)+(suma%10);
		System.out.println("Tu n�mero de la suerte es: "+suerte+" "+suma);
		input.close();
	}

}
