package ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Introduce una cadena de texto:");
		String cadena=input.nextLine();
		String a="a";
		String e="e";
		String i="i";
		String o="o";
		String u="u";
		String A="A";
		String E="E";
		String I="I";
		String O="O";
		String U="U";      
		System.out.println(cadena.contains(a)||cadena.contains(e)||cadena.contains(i)||cadena.contains(o)||cadena.contains(u)||cadena.contains(A)||cadena.contains(E)||cadena.contains(I)||cadena.contains(O)||cadena.contains(U)?"Tienene un vocal":"No tiene ninguna vocal.");
		System.out.println(cadena.startsWith(a)||cadena.startsWith(e)||cadena.startsWith(i)||cadena.startsWith(o)||cadena.startsWith(u)||cadena.startsWith(A)||cadena.startsWith(E)||cadena.startsWith(I)||cadena.startsWith(O)||cadena.startsWith(U)?"La cadena empieza con vocal":"La cadena no empieza por vocal");
		System.out.println(cadena.endsWith(a)||cadena.endsWith(e)||cadena.endsWith(i)||cadena.endsWith(o)||cadena.endsWith(o)||cadena.endsWith(u)||cadena.endsWith(A)||cadena.endsWith(E)||cadena.endsWith(I)||cadena.endsWith(O)||cadena.endsWith(U)?"La cadena termina en vocal":"La cadena no termina en vocal");
		input.close();
	}

}
