package ejercicio15;

import java.util.Scanner;

public class Ejercicio15B {

	public static void main(String[] args) {
		Scanner input=new Scanner (System.in);
		
		System.out.println("Introduce una cadena de texto separada por un solo espacio");
		String cadena=input.nextLine();
		System.out.println(cadena.substring(cadena.indexOf(' ')+1)+" "+cadena.substring(0,cadena.indexOf(' ')));
		
		input.close();

	}

}
