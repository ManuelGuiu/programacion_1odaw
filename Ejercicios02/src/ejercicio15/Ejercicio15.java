package ejercicio15;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Introduce una cadena de dos palabras separadas por un espacio");
		String cadena=input.nextLine();
		
		int numero=cadena.indexOf(" ");
		int numero2=numero+1;
		System.out.println(cadena.substring(numero2)+" "+cadena.substring(0,numero));
		
		input.close();

	}

}
