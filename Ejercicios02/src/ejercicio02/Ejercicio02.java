package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("introduce una cadena de texto");
		String cadena = input.nextLine();
		String muestra = cadena.replaceAll(" ","");
		System.out.println(muestra);
		
		input.close();
	}

}
