package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		Scanner input= new Scanner(System.in);
		
		System.out.println("Introduce una cadena de texto");
		String cadena1=input.nextLine();
		System.out.println("Introduce una segunda cadena de texto");
		String cadena2=input.nextLine();
		boolean cadena3=cadena1.equals(cadena2);
		System.out.println(cadena3?"Las cadenas son iguales":"Las cadenas no son iguales");
		input.close();

	}

}
