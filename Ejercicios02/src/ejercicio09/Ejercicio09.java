package ejercicio09;

import java.util.Scanner;

public class Ejercicio09 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		int numero=input.nextInt();
		System.out.println(numero%10);
		System.out.println(numero%100);
		System.out.println(numero%1000);
		System.out.println(numero%10000);
		System.out.println(numero);
		input.close();

	}

}
