package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Introduce el primer cateto:");
		double cateto1=input.nextDouble();
		System.out.println("Introduce el segundo cateto:");
		double cateto2=input.nextDouble();
		double raiz=cateto1*cateto1+cateto2*cateto2;
		System.out.println(Math.sqrt(raiz));
		input.close();
	}

}
