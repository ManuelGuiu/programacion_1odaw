package vectores;

public class MisVectores {

	public static void main(String[] args) {
		
		//creamos un vector
		int miVector[] = {1,2,3,4,5,6,7,8,9,10};
		//imprimimos las componentes del vector
		for (int i = 0; i < 10; i++) {
			System.out.println("El componente "+ i + " del vector es: " + miVector[i]);
		}
		
		
		//cambiamos el componente 2 del vector
		miVector[2] = 25;
		//cambiamos el componente 7 del vector
		miVector[7] = 33;
		//imprimimos las componentes del vector
		for (int i = 0; i < 10; i++) {
		System.out.println("El componente "+ i + " del vector es: " + miVector[i]);
		}
		
		
		//sumamos los componentes de un vector
		int suma = 0;
		for (int i = 0; i < 10; i++) {
			suma = suma + miVector[i];
		}
		System.out.println("La suma de los vectores es " + suma);
	}

}
