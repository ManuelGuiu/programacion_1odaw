package vectores;

import java.util.Scanner;

public class MisVectores4NoVoid {

		public static void main(String[] args) {
			int [] miVector = insertarDatosArray();
			sumarArray(miVector);
			maxArray(miVector);
			promedioArray(miVector);
		}
		
		public static int[] insertarDatosArray(){
			Scanner input = new Scanner(System.in);
			System.out.println("Dame la longitud del vector");
			int vector[] = new int[input.nextInt()];
			
			for(int i = 0; i < vector.length; i++) {
				System.out.println("Introduce el componenete " + i + " del vector.");
				vector[i] = input.nextInt();
			}
			input.close();
			return vector;
		}
		public static int sumarArray(int[] vector){
			int suma = 0;
			for (int i = 0; i < vector.length; i++) {
				suma = suma + vector[i];
			}
			System.out.println("La suma de los componentes es: " + suma);
			return suma;
		}
		public static int maxArray(int[] vector){
			int maximo = 0;
			for (int i = 0; i < vector.length; i++) {
				if(vector[i] > maximo) {
					maximo = vector[i];
				}
			}
			System.out.println("El m�ximo es: " + maximo);
			return maximo;
		}
		public static int promedioArray(int[] vector){
			int media = 0;
			for (int i = 0; i < vector.length; i++) {
				media = media + vector[i];
			}
			media = media / vector.length;
			System.out.println("La media es: " + media);
			return media;
		}

	}