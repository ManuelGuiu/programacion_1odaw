package vectores;

import java.util.Scanner;

public class MisVectores1 {

	public static void main(String[] args) {
		//declaramos un scanner
		Scanner input = new Scanner (System.in);
		
		//declaramos vector y su tama�o
		int miVector[] = new int[10];
		int miVector1[] = new int[10];
		
		//pedimos los datos
		for (int i = 0; i < 10; i++) {
			System.out.println("Dame la componente " + i + " del vector");
			miVector[i] = input.nextInt();
		}
		for (int i = 0; i < 10; i++) {
			System.out.println("La componente " + i + " del vector es: " + miVector[i]);
		}
		int suma = 0;
		for (int i = 0; i < 10; i++) {
			suma = suma +miVector[i];
		}
		System.out.println("La suma de los vectores es " + suma);
		
		for (int i = 0; i < 10; i++) {
			miVector1[i] = miVector[i];
			System.out.println(miVector1[i]);
		}
		
		//////////////////////////////////////////////////////////////////////////////////
		input.nextLine();
		String miVector3[] = new String[5];
		for (int i = 0; i < 5; i++) {
			System.out.println("Dame la componente " + i + " del vector");
			miVector3[i] = input.nextLine();
		}
		for (int i = 0; i < 5; i++) {
			System.out.println("La componente " + i + " del vector es: " + miVector3[i]);
		}
		///////////////////////////////////////////////////////////////////////////////////
		 
		double miVector4[] = new double[5];
		for (int i = 0; i < 5; i++) {
			System.out.println("Dame la componente " + i + " del vector");
			miVector4[i] = input.nextDouble();
		}
		double media = 0;
		for (int i = 0; i < 5; i++) {
			media = media + miVector4[i];
		}
		media = media / 5;
		System.out.println("La media es " + media);
		
		input.close();
	}

}
