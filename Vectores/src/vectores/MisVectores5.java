package vectores;

import java.util.Scanner;

public class MisVectores5 {
	// declarar un vector de 30 enteros y obtener el n pares y el n impares.
	// declarar un vector de 30 enteros y obtener la suma de pares e impares.
	// declarar un vector de 20 caracteres y contabilizar cuantos de ellos son caracteres numericos.
	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {

		int vectorReal[] = leerVector();
		System.out.println("El numero de pares es " + contadorPar(vectorReal));
		System.out.println("El numero de impares es " + contadorImpar(vectorReal));
		System.out.println("La suma de los pares es " + sumaPar(vectorReal));
		System.out.println("La suma de los impares es " + sumaImpar(vectorReal));

		String[] vectorChar = leerString();
		System.out.println("El numero de caracteres numericos es: " + caracteres(vectorChar));

	}

	public static int[] leerVector() {
		
		System.out.println("La longitud del vector es 30");
		int vector[] = new int[3];
		for (int i = 0; i < vector.length; i++) {
			System.out.println("Introduce el valor" + i + " del vector:");
			vector[i] = input.nextInt();
		}
		input.nextLine();
		return vector;
	}

	public static int contadorPar(int[] vector) {
		int contador = 0;
		for (int i = 0; i < vector.length; i++) {
			if (vector[i] % 2 == 0) {
				contador++;
			}
		}
		return contador;
	}

	public static int contadorImpar(int[] vector) {
		int contador = 0;
		for (int i = 0; i < vector.length; i++) {
			if (vector[i] % 2 != 0) {
				contador++;
			}
		}
		return contador;
	}

	public static int sumaPar(int[] vector) {
		int suma = 0;
		for (int i = 0; i < vector.length; i++) {
			if (vector[i] % 2 == 0) {
				suma = suma + vector[i];
			}
		}
		return suma;
	}

	public static int sumaImpar(int[] vector) {
		int suma = 0;
		for (int i = 0; i < vector.length; i++) {
			if (vector[i] % 2 != 0) {
				suma = suma + vector[i];
			}
		}
		return suma;
	}

	public static String[] leerString() {

		System.out.println("La longitud del vector es 20 introduce caracteres.");
		String vector[] = new String[5];
		for (int i = 0; i < vector.length; i++) {
			System.out.println("Introduce el valor " + (i + 1) + " del vector:");
			vector[i] = input.nextLine();
			if (vector[i].length() > 1) {
				System.out.println("Introduce un solo caracter.");
				i--;
			}
		}
		return vector;
	}

	public static int caracteres(String[] vector) {

		String cadena;
		int contador = 0;
		for (int i = 0; i < 5; i++) {
			cadena = vector[i];
			if (cadena.charAt(0) >= '0' && cadena.charAt(0) <= '9') {
				contador++;
			}
		}

		return contador;
	}

}
