package vectores;

import java.util.Scanner;

public class MisVectores3 {
	
	static Scanner input = new Scanner (System.in);

	public static void main(String[] args) {
		//pide datos vector y suma componentes
		//pide datos vector y muestra media
		//pide datos vector y muestra minimo
		//pide datos vector y muestra maximo
		
		int menu = 0;
		
		do {
			System.out.println("Seleccionna una de las opciones:");
			System.out.println("1. pide datos vector y suma componentes\r\n" + 
					"2. pide datos vector y muestra media\r\n" + 
					"3. pide datos vector y muestra minimo\r\n" + 
					"4. pide datos vector y muestra maximo\r\n" +
					"5. salir");
			menu = input.nextInt();
			
			switch(menu) {
				case 1:
					int vectorSuma[] = new int[3];
					int suma = 0;
					for(int i = 0; i < vectorSuma.length; i++) {
						System.out.println("Introduce el componente " + i + " del vector.");
						vectorSuma[i] = input.nextInt();
						suma = suma + vectorSuma[i];
					}
					System.out.println("La suma de los componentes es: " + suma);
					break;
				case 2:
					int vectorMedia[] = new int[3];
					int media = 0;
					for(int i = 0; i < vectorMedia.length; i++) {
						System.out.println("Introduce el componente " + i + " del vector.");
						vectorMedia[i] = input.nextInt();
						media = media + vectorMedia[i];
					}
					media = media / vectorMedia.length;
					System.out.println("La media es: " + media);
					break;
				case 3:
					int vectorMinimo[] = new int[3];
					int minimo = vectorMinimo[0];
					for(int i = 0; i < vectorMinimo.length; i++) {
						System.out.println("Introduce el componente " + i + " del vector.");
						vectorMinimo[i] = input.nextInt();
						if (vectorMinimo[i] < minimo) {
							minimo = vectorMinimo[i];
						}
					}
					
					break;
				case 4:
					int vectorMaximo[] = new int[3];
					int maximo = vectorMaximo[0];
					for(int i = 0; i < vectorMaximo.length; i++) {
						System.out.println("Introduce el componente " + i + " del vector.");
						vectorMaximo[i] = input.nextInt();
						if (vectorMaximo[i] < maximo) {
							maximo = vectorMaximo[i];
						}
					}
					break;
				case 5:
					System.exit(0);
					break;
				default:
					System.out.println("Elige una de las opciones mostradas");
			}
		}while(menu != 5);

	}

}
