package vectores;

import java.util.Scanner;

public class MisVectores4Void {
	
	static Scanner input = new Scanner(System.in);
	static int vector[] = new int[input.nextInt()];

	public static void main(String[] args) {
		insertarDatosArray();
		sumarArray();
		maxArray();
		promedioArray();
	}
	
	public static void insertarDatosArray(){
		
		for(int i = 0; i < vector.length; i++) {
			System.out.println("Introduce el componenete " + i + " del vector.");
			vector[i] = input.nextInt();
		}
	}
	public static void sumarArray(){
		int suma = 0;
		for (int i = 0; i < vector.length; i++) {
			suma = suma + vector[i];
		}
		System.out.println("La suma de los componentes es: " + suma);
	}
	public static void maxArray(){
		int maximo = 0;
		for (int i = 0; i < vector.length; i++) {
			if(vector[i] > maximo) {
				maximo = vector[i];
			}
		}
		System.out.println("El m�ximo es: " + maximo);
	}
	public static void promedioArray(){
		int media = 0;
		for (int i = 0; i < vector.length; i++) {
			media = media + vector[i];
		}
		media = media / vector.length;
		System.out.println("La media es: " + media);
	}

}
