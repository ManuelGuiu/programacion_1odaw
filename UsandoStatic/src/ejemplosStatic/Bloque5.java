package ejemplosStatic;

public class Bloque5 {
	
	static int a;
	static int b;
	static {
		a = 10;
		b = 20;
	}
	
	public static void manin (String[] args) {
		System.out.println("El valor de a es: " + a);
		System.out.println("El valor de b es: " + b);
	}
	
}
