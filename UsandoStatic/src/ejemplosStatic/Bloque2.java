package ejemplosStatic;

public class Bloque2 {
	
	static int a = 10;
	static int b = 20;
	static int c = 30;
	
	public static void mostrarValores() {
		System.out.println("El valor de a es: " + a);
		System.out.println("El valor de b es: " + b);
	}
	
	public static int sumarValores() {
		int x = 4;
		int y = 7;
		int suma = x + y;
		return suma;
	}
	
	public static void main (String[] args) {
		//metodo void, imprimo dentro del metodo mostrarValores
		Bloque2.mostrarValores();
		//metodo int, imprimo en el main
		//no guarda el resultado
		System.out.println(Bloque2.sumarValores());
		//asi se guarda el resultado
		int miSuma = Bloque2.sumarValores();
		System.out.println(miSuma);
	}
	
	
}
