package ejemplosStatic;

import java.util.Scanner;

public class Circulo {
	
	static final double NPI = 3.1416;
	
	public static double calcularAreaCirculo(double radio) {
		double area = radio * radio * NPI;
		return area;
	}
	
	public static double calcularLongitudCirculo(double radio) {
		double longitud = radio * NPI * 2;
		return longitud;
	}
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Radio");
		double ra = input.nextDouble();
		
		System.out.println("Area del circulo " + Circulo.calcularAreaCirculo(ra));
		System.out.println("Longitud del circulo " + Circulo.calcularLongitudCirculo(ra));
		
		
		input.close();
	}
	
	
}
