package ejemplosStatic;

import java.util.Scanner;

public class Lectura {
	static Scanner input = new Scanner(System.in);
	
	public static int leerEntero() {
		
		System.out.println("Introduce un entero");
		int n = input.nextInt();
		//input.close();
		return n;
	}
	
	public static double leerDouble() {
		
		System.out.println("Introduce un double");
		double n = input.nextDouble();
		//input.close();
		return n;
	}
	
	public static void main(String[] args) {
		int num = Lectura.leerEntero();
		System.out.println("Multiplicado por 3 es : " + num * 3);
		
		double num2 = Lectura.leerDouble();
		System.out.println("Divido entre 5 es " + num2/5);
	}
}
