package ejemplosStatic;

import java.util.Scanner;

public class Bloque4 {
	
	public static void mostrarValores(int a, int b) {
		System.out.println("El valor de a es: " + a);
		System.out.println("El valor de b es: " + b);
	}
	
	public static void main (String[] args) {
		Scanner entrada = new Scanner (System.in);
		
		System.out.println("Dame un numero");
		int x = entrada.nextInt();
		System.out.println("Dame otro numero");
		int y = entrada.nextInt();
		entrada.nextLine();
		
		Bloque4.mostrarValores(x, y);
		
		entrada.close();
	}
	
	
}
