package ejemplosStatic;

import java.util.Scanner;

public class OperacionLector {
	static Scanner entrada = new Scanner(System.in);

	public static int sumar(int x1, int x2) {
		int s = x1 + x2;
		return s;
	}
	
	public static int restar(int x1, int x2) {
		int r = x1 - x2;
		return r;
	}
	
	public static double dividir(int x1, double x2) {
		double d = x1 / x2;
		return d;
	}
	
	public static int multiplicar(int x1, int x2) {
		int m = x1 * x2;
		return m;
	}
	
	public static int Lectura() {		
		System.out.println("Introduce numero");
		int n = entrada.nextInt();
		
		return n;
	}
	
	public static void main(String[] args) {
		int num1 = OperacionLector.Lectura();
		int num2 = OperacionLector.Lectura();
		
		System.out.println("La resta de " + num1 + " y " + num2 + " es:");
		System.out.println(OperacionLector.restar(num1, num2));
		
		System.out.println("La suma de " + num1 + " y " + num2 + " es:");
		System.out.println(OperacionLector.sumar(num1, num2));
		
		System.out.println("La division de " + num1 + " y " + num2 + " es:");
		System.out.println(OperacionLector.dividir(num1, num2));
		
		System.out.println("La multiplicacion de " + num1 + " y " + num2 + " es:");
		System.out.println(OperacionLector.multiplicar(num1, num2));
	}	
}