package ejemplosStatic;

public class ClaseMath {

	public static void main(String[] args) {
		System.out.println("Valor de pi " + Math.PI);
		System.out.println("5 elevado a 3 " + Math.pow(5, 3));
		System.out.println("Maximo de 6 y 9 " + Math.max(6, 9));
	}

}
