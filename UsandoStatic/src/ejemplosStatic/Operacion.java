package ejemplosStatic;

public class Operacion {
	
	public static int sumar(int x1, int x2) {
		int s = x1 + x2;
		return s;
	}
	
	public static int restar(int x1, int x2) {
		int r = x1 - x2;
		return r;
	}
	
	public static double dividir(int x1, double x2) {
		double d = x1 / x2;
		return d;
	}
	
	public static int multiplicar(int x1, int x2) {
		int m = x1 * x2;
		return m;
	}
	
	public static void main(String[] args) {
		System.out.println("La resta de 2 y 4 es:");
		System.out.println(Operacion.restar(2, 4));
		
		System.out.println("La suma de 2 y 4 es:");
		System.out.println(Operacion.sumar(2, 4));
		
		System.out.println("La division de 2 y 4 es:");
		System.out.println(Operacion.dividir(2, 4));
		
		System.out.println("La multiplicacion de 2 y 4 es:");
		System.out.println(Operacion.sumar(2, 4));
	}
	
	
}
