package ejemplosStatic;

import java.util.Scanner;

public class ClaseMathLectura {
	static Scanner input = new Scanner(System.in);
	
	public static int leerEntero() {
		System.out.println("Introduce un entero");
		int n = input.nextInt();	
		return n;
	}
	
	public static double leerDouble() {
		System.out.println("Introduce un double");
		double n = input.nextDouble();
		return n;
	}
	
	public static void main(String[] args) {
		System.out.println("Calcular potencia");
		System.out.println("Pedir exponente");
		int exponente = ClaseMathLectura.leerEntero();
		System.out.println("Pedir base");
		int base = ClaseMathLectura.leerEntero();
		System.out.println("La potencia es " + Math.pow(base, exponente));
		
		System.out.println("Maximo");
		System.out.println("Primero");
		double num1 = ClaseMathLectura.leerDouble();
		System.out.println("Segundo");
		double num2 = ClaseMathLectura.leerDouble();
		System.out.println("El maximo es " + Math.max(num1, num2));
	}
	
}