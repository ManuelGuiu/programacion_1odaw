package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		
		System.out.println("Introduce el mes en valor numerico 1-12");
		int mes=input.nextInt();
		
		/*1=enero=31 2febrero=28 3=marzo=31 4=abril=30 5=mayo=31 6=junio=30 
		 *7julio31 8agosto31 9septiembre30 10octubre31 11novembre30 12dicienmbre31*/
		boolean dia1= mes==1||mes==3||mes==5||mes==7||mes==8||mes==10||mes==12;
		boolean dia2= mes==2;
		boolean dia3= mes==4||mes==6||mes==9||mes==11;
		
		if(dia1) {
			System.out.println("Mes de 31");
		}else if(dia2) {
			System.out.println("Mes de 28");
		}else if(dia3) {
			System.out.println("Mes de 30");
		}else if(mes<0||mes>12) {
			System.out.println("No se cumplen los parametros");
		}
		
		input.close();

	}

}
