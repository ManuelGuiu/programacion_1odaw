package ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		
		System.out.println("Introduce la hora:");
		int hora=input.nextInt();
		
		System.out.println("Introduce los minutos:");
		int minuto=input.nextInt();
		
		System.out.println("Introduce los segundos:");
		int segundo=input.nextInt();
		
		if(hora>=0&&hora<=23 && minuto>=0&&minuto<=59 && segundo>=0&&segundo<=59) {
			System.out.println(hora+":"+minuto+":"+segundo);
		}else {System.out.println("La hora no cumple el formato");}
		
		input.close();

	}

}
