package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		Scanner input =new Scanner(System.in);
		
		System.out.println("Introduce un caracter");
		char caracter1=input.nextLine().charAt(0);
		
		System.out.println("Introduce otro caracter:");
		char caracter2=input.nextLine().charAt(0);
		
		if (caracter1>=97&&caracter1<=122&&caracter2>=97&&caracter2<=122) {
			
			System.out.println("Son minusculas");
			
		}else {System.out.println("No lo son");}
		
		input.close();

	}

}
