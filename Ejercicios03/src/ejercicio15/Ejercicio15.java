package ejercicio15;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		
		System.out.println("introduce una hora en formato hh:mm:ss");
		String hora=input.nextLine();
		
		
		int posicion1=hora.indexOf(':');
		String hora2=hora.substring(posicion1+1);
		
		int posicion2=hora2.indexOf(':');
		String segundos=hora2.substring(posicion2+1);
		
		String horareal=hora.substring(0,posicion1);
		String minutos=hora2.substring(0,posicion2);
		
		int horaNum=Integer.parseInt(horareal);
		int minutoNum=Integer.parseInt(minutos);
		int segundoNum=Integer.parseInt(segundos);
		
		if (horaNum>=0&&horaNum<=23) {
			if (minutoNum>=0&&minutoNum<=59) {
				if(segundoNum>=0&&segundoNum<=59) {
					System.out.println("El formatod de la hora es correcto");
				}else {
					System.out.println("No se cumple el formato en segundos de 0-59s");
					}
			}else {
				System.out.println("No se cumple el formato en minutos de 0-59m");
				}
		}else {
			System.out.println("No se cumple el formato de hora entre 0-23h");
			}
		
		/*------------------------------------------------------------------------*/
		
		System.out.println("Introduce una fecha de formato num�rico d�a/mes/a�o");
		String fechaPedida=input.nextLine();
		
		int lugar1=fechaPedida.indexOf('/');
		String fecha1=fechaPedida.substring(lugar1);
		
		int lugar2=fecha1.indexOf('/')+1;
		String fecha2=fecha1.substring(lugar2);
		
		int lugar3=fecha2.indexOf('/')+1;
		String a�o=fecha2.substring(lugar3);
		
		String dia=fechaPedida.substring(0,lugar1);
		
		String mes=fecha2.substring(0,lugar3-1);
		
		int diaNum=Integer.parseInt(dia);
		int mesNum=Integer.parseInt(mes);
		int a�oNum=Integer.parseInt(a�o);
		
		if(diaNum>=1&&diaNum<=30) {
			if (mesNum>=1&&mesNum<=12) {
				System.out.println("El formato es correcto "+diaNum+mesNum+a�oNum);
			}else {
				System.out.println("El formato mes no cumple los requisitos 1-12");
				}
		}else {
			System.out.println("El formato del d�a no cumple los requisitos 1-30");
			}
		
		input.close();

	}

}
