package ejercicio08;

import java.util.Scanner;

public class Ejercicio08 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Introduce dividendo");
		int numero1=input.nextInt();
		System.out.println("Introduce divisor");
		int numero2=input.nextInt();
		if(numero2==0) {
			System.out.println("El divisor es cero");
		}else {
			System.out.println(numero1/numero2);
		}
		input.close();
	}

}
