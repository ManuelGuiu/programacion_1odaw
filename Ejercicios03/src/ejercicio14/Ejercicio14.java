package ejercicio14;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Introduce tu peso");
		double peso=input.nextDouble();
		System.out.println("Introduce tu altura en m");
		double altura=input.nextDouble();
		
		double imc=peso/(altura*altura);
		System.out.println(imc);
		
		if(imc<16) {
			System.out.println("Criterio de ingreso en hospital");
			System.out.println(imc);
		}else if(imc>=16&&imc<17) {
			System.out.println("Infrapeso");
			System.out.println(imc);
		}else if(imc>=17&&imc<18) {
			System.out.println("Bajo peso");
			System.out.println(imc);
		}else if(imc>=18&&imc<25) {
			System.out.println("Peso normal(saludable)");
			System.out.println(imc);
		}else if(imc>=25&&imc<30) {
			System.out.println("sobrepeso(obesidad de grado I)");
			System.out.println(imc);
		}else if(imc>=30&&imc<35) {
			System.out.println("sobre peso cr�nico(obesidad de grado II)");
			System.out.println(imc);
		}else if(imc>=35&&imc<40) {
			System.out.println("obesidad prem�rbida(obesidad de grado III)");
			System.out.println(imc);
		}else if(imc>=40) {
			System.out.println("obesidad m�rbida(obesidad de grado IV)");
			System.out.println(imc);
		}
		
		input.close();
	}

}
