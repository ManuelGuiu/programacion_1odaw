package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {
		Scanner input =new Scanner(System.in);
		System.out.println("introduce un numero");
		char caracter=input.nextLine().charAt(0);
		if(caracter>=48&&caracter<=57) {
			System.out.println("Es un numero entre 0 y 9");
		}else {System.out.println("No es un numero entre 0 y 9");}
		input.close();

	}

}
