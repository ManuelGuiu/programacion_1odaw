package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Introduce una cadena");
		String cadena1=input.nextLine();
		System.out.println("Introduce una segunda cadena");
		String cadena2=input.nextLine();
		
		boolean igual=cadena1.equalsIgnoreCase(cadena2);
		
		if (igual) {
			System.out.println("Las cadena son iguales");
		}else {System.out.println("Las cadenas no son iguales");}
		
		input.close();
	}

}
