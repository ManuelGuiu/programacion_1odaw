package ejercicio05;

public class Principal {

	public static void main(String[] args) {
		Vehiculo ch1 = new Vehiculo();
		ch1.setMarca("Dodge");
		ch1.setModelo("Challenger");
		ch1.setAutonomia(300);
		ch1.setKilometraje(26000);
		
		System.out.println("Marca: " + ch1.getMarca());
		System.out.println("Modeli: " + ch1.getModelo());
		System.out.println("Autonomia: " + ch1.getAutonomia());
		System.out.println("Kilometraje: " + ch1.getKilometraje());
		
		System.out.println(ch1.esSeguro(ch1.getKilometraje()));
	}

}
