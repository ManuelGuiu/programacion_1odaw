package ejercicio05;

public class Vehiculo {
	private String marca;
	private String modelo;
	private int autonomia;
	private int kilometraje;
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public int getAutonomia() {
		return autonomia;
	}
	public void setAutonomia(int autonomia) {
		this.autonomia = autonomia;
	}
	public int getKilometraje() {
		return kilometraje;
	}
	public void setKilometraje(int kilometraje) {
		this.kilometraje = kilometraje;
	}
	
	boolean esSeguro(int kilometros) {
		if(kilometros < this.autonomia) {
			return true;
		}else {
			return false;
		}
	}
}
