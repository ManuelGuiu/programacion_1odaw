package ejercicio04;

public class Principal {
	final static float IVA = 0.21F;
	public static void main(String[] args) {
		Libros l1 = new Libros();
		l1.setTitulo("Octubre rojo");
		l1.setAutor("Tus muelas");
		l1.setAņoPublicacion(2010);
		l1.setEditorial("San pepe");
		l1.setPrecio(2.20F);
		System.out.println("Titulo " + l1.getTitulo());
		System.out.println("Autor " + l1.getAutor());
		System.out.println("Aņo de publicacion " + l1.getAņoPublicacion());
		System.out.println("Editorila " + l1.getEditorial());
		System.out.println("Precio " + l1.getPrecio());
		
		Libros l2 = new Libros();
		l2.setTitulo("Octubre rojo");
		l2.setAutor("Tus muelas");
		l2.setAņoPublicacion(2010);
		l2.setEditorial("San pepe");
		l2.setPrecio(2.20F);
		System.out.println("Titulo " + l2.getTitulo());
		System.out.println("Autor " + l2.getAutor());
		System.out.println("Aņo de publicacion " + l2.getAņoPublicacion());
		System.out.println("Editorila " + l2.getEditorial());
		System.out.println("Precio " + l2.getPrecio());
		
		System.out.println(Libros.PrecioConIva(l1.getPrecio(), IVA));
	}

}
