package ejercicio01;

import java.util.Scanner;

public class Principal {
	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		Profesor prof1 = new Profesor();
		Profesor prof2 = new Profesor();
		Profesor prof3 = new Profesor();
		Profesor prof4 = new Profesor();
		
		rellenarProfesor(prof1);
		rellenarProfesor(prof2);
		rellenarProfesor(prof3);
		rellenarProfesor(prof4);
		
		visualizarProfesor(prof1);
		visualizarProfesor(prof2);
		visualizarProfesor(prof3);
		visualizarProfesor(prof4);
	}
	
	static Profesor rellenarProfesor(Profesor prof) {
		System.out.println("Introduce el nombre del profesor");
		prof.setNombre(input.nextLine());
		System.out.println("Introduce el apellido del profesor");
		prof.setApellido(input.nextLine());
		System.out.println("Introduce el ciclo que imparte el profesor");
		prof.setCiclo(input.nextLine());
		return prof;
	}
	
	static Profesor visualizarProfesor(Profesor prof) {
		System.out.println(prof.getNombre());
		System.out.println(prof.getApellido());
		System.out.println(prof.getCiclo());
		return prof;
	}

}
