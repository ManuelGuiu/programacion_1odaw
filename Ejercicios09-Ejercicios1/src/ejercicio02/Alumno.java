package ejercicio02;

import java.util.Scanner;

public class Alumno {
	private String nombre;
	private String apellidos;
	private double notaMedia;
	static Scanner input = new Scanner(System.in);
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public double getNotaMedia() {
		return notaMedia;
	}
	public void setNotaMedia(double notaMedia) {
		this.notaMedia = notaMedia;
	}
	
	public static void promocion(double nota) {
		if(nota >= 5) {
			System.out.println("El alumno promociona");
		}else {
			System.out.println("El alumno no promociona");
		}
	}
	
	static Alumno rellenarAlumno(Alumno alum) {
		System.out.println("Introduce nombre del alumno");
		alum.setNombre(input.nextLine());
		System.out.println("Introduce apellido del alumno");
		alum.setApellidos(input.nextLine());
		double nota = -1;
		do {
			System.out.println("Introduce la media");
			nota = input.nextDouble();
			if(nota < 0 || nota > 10) {
				System.out.println("Introduce un valor valido");
			}else {
				alum.setNotaMedia(nota);
			}
		}while(nota < 0 || nota > 10);
		return alum;
	}
	
	static Alumno visualizarAlumno(Alumno alum) {
		System.out.print("Nombre del alumno: ");
		System.out.println(alum.getNombre());
		System.out.print("Apellidos del alumno: ");
		System.out.println(alum.getApellidos());
		System.out.print("Nota media: ");
		System.out.println(alum.getNotaMedia());
		promocion(alum.getNotaMedia());
		return alum;
	}
}
