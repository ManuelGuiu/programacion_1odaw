package ejercicio02;

public class Principal {
	
	public static void main(String[] args) {
		
		Alumno alum1 = new Alumno();
		
		Alumno.rellenarAlumno(alum1);
		Alumno.visualizarAlumno(alum1);
	}
	
}
