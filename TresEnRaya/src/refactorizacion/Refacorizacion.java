package refactorizacion;

import java.util.Scanner;

public class Refacorizacion {
	public static void main(String args[]) {
		//Scanner renombrado
		Scanner input = new Scanner(System.in);
		//String pasado a minusculas
		String tablero[] = new String[9];
		//String pasado a minusculas
		String jugador = "X";
		//String pasado a minusculas
		String ganador = null;
		
		//valor cambiado a 0
		int contador = 0;
		
		//int declarado en bucle for y renombrado
		for (int i = 0; i < 9; i++) {
			tablero[i] = String.valueOf(i + 1);
		}

		System.out.println("Bienvenido al 3x3.");
		System.out.println("--------------------------------");
		System.out.println("/---|---|---\\");
		System.out.println("| " + tablero[0] + " | " + tablero[1] + " | " + tablero[2] + " |");
		System.out.println("|-----------|");
		System.out.println("| " + tablero[3] + " | " + tablero[4] + " | " + tablero[5] + " |");
		System.out.println("|-----------|");
		System.out.println("| " + tablero[6] + " | " + tablero[7] + " | " + tablero[8] + " |");
		System.out.println("/---|---|---\\");
		System.out.println("X Primero. Introduce el numero en el que quieres poner la X:");

		while (ganador == null) {
			//cambio nombre de variable int
			int numeroLeido = input.nextInt();
			if (tablero[numeroLeido - 1].equals(String.valueOf(numeroLeido))) {
				tablero[numeroLeido - 1] = jugador;
				if (jugador.equals("X")) {
					jugador = "O";
				} else {
					jugador = "X";
				}

				System.out.println("/---|---|---\\");
				System.out.println("| " + tablero[0] + " | " + tablero[1] + " | " + tablero[2] + " |");
				System.out.println("|-----------|");
				System.out.println("| " + tablero[3] + " | " + tablero[4] + " | " + tablero[5] + " |");
				System.out.println("|-----------|");
				System.out.println("| " + tablero[6] + " | " + tablero[7] + " | " + tablero[8] + " |");
				System.out.println("/---|---|---\\");
				//a�adido mensaje
				System.out.println("Introduce el numero en el que quieres poner la ficha:");

				for (int a = 0; a < 8; a++) {
					String Linea = null;
					switch (a) {
					case 0:
						Linea = tablero[0] + tablero[1] + tablero[2];
						break;
					case 1:
						Linea = tablero[3] + tablero[4] + tablero[5];
						break;
					case 2:
						Linea = tablero[6] + tablero[7] + tablero[8];
						break;
					case 3:
						Linea = tablero[0] + tablero[3] + tablero[6];
						break;
					case 4:
						Linea = tablero[1] + tablero[4] + tablero[7];
						break;
					case 5:
						Linea = tablero[2] + tablero[5] + tablero[8];
						break;
					case 6:
						Linea = tablero[0] + tablero[4] + tablero[8];
						break;
					case 7:
						Linea = tablero[2] + tablero[4] + tablero[6];
						break;
					}
					if (Linea.equals("XXX")) {
						ganador = "X";
					} else if (Linea.equals("OOO")) {
						ganador = "O";
					}
				}

				for (int a = 0; a < tablero.length; a++) {
					if (tablero[a].contentEquals("X") || tablero[a].contentEquals("O")) {

						contador++;
						//valor cambiado a 9
						if (contador == 9) {
							ganador = "empate";
						}
						break;
					}

				}

			} else {
				System.out.println("La casilla ya esta ocupada ocupada");
				continue;
			}
		}
		if (ganador.equalsIgnoreCase("empate")) {
			System.out.println("EMPATE!!!");
		} else {
			System.out.println("enhorabuena! han ganado las: " + ganador);
		}

		input.close();
	}
}
