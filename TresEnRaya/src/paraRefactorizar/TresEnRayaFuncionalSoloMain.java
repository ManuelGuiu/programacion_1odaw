package paraRefactorizar;

import java.util.Scanner;

public class TresEnRayaFuncionalSoloMain {

	public static void main(String[] args) {
		String[][] tablero = new String[3][3];
		Scanner input = new Scanner(System.in);
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[i].length; j++) {
				tablero[i][j] = "-";
			}
		}
		String jugador;

		do {
			System.out.println("Introduce tu jugador (X o 0)");
			jugador = input.nextLine();

		} while (jugador.charAt(0) != 'X' && jugador.charAt(0) != '0');
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[i].length; j++) {
				System.out.print(tablero[i][j] + " ");
				if (j == tablero[i].length - 1) {
					System.out.println();
				}
			}
		}
		boolean victoria = true;
		int contador = 0;
		boolean existe = true;
		do {
			if (jugador.charAt(0) == 'X') {
				System.out.println("Introduce las coordenadas para introducir el valor");
				do {
					System.out.println("Coordenada x");
					int coorX = input.nextInt();
					System.out.println("Coordenada y");
					int coorY = input.nextInt();
					input.nextLine();

					if (tablero[coorX][coorY] == "X" || tablero[coorX][coorY] == "0") {
						System.out.println("Ya existe un dato");
						existe = true;
						for (int i = 0; i < tablero.length; i++) {
							for (int j = 0; j < tablero[i].length; j++) {
								System.out.print(tablero[i][j] + " ");
								if (j == tablero[i].length - 1) {
									System.out.println();
								}
							}
						}
					} else {
						tablero[coorX][coorY] = "X";
						contador++;
						existe = false;
					}

				} while (existe);

				int randomX;
				int randomY;

				do {
					randomX = (int) (Math.random() * (3 - 0) + 0);
					randomY = (int) (Math.random() * (3 - 0) + 0);
					if (tablero[randomX][randomY] == "X" || tablero[randomX][randomY] == "0") {
						existe = true;
					} else if (tablero[randomX][randomY] == "-") {
						tablero[randomX][randomY] = "0";
						existe = false;
						contador++;
					} else if (contador == 10) {
						existe = false;
					}
				} while (existe);
				for (int i = 0; i < tablero.length; i++) {
					for (int j = 0; j < tablero[i].length; j++) {
						System.out.print(tablero[i][j] + " ");
						if (j == tablero[i].length - 1) {
							System.out.println();
						}
					}
				}

			} else if (jugador.charAt(0) == '0') {
				System.out.println("Introduce las coordenadas para introducir el valor");

				do {
					System.out.println("Coordenada x");
					int coorX = input.nextInt();
					System.out.println("Coordenada y");
					int coorY = input.nextInt();
					input.nextLine();

					if (tablero[coorX][coorY] == "X" || tablero[coorX][coorY] == "0") {
						System.out.println("Ya existe un dato");
						existe = true;
						for (int i = 0; i < tablero.length; i++) {
							for (int j = 0; j < tablero[i].length; j++) {
								System.out.print(tablero[i][j] + " ");
								if (j == tablero[i].length - 1) {
									System.out.println();
								}
							}
						}
					} else {
						tablero[coorX][coorY] = "0";
						contador++;
						existe = false;
					}

				} while (existe);

				int randomX;
				int randomY;
				do {
					randomX = (int) (Math.random() * (3 - 0) + 0);
					randomY = (int) (Math.random() * (3 - 0) + 0);
					if (tablero[randomX][randomY] == "X" || tablero[randomX][randomY] == "0") {
						existe = true;
					} else if (tablero[randomX][randomY] == "-") {
						tablero[randomX][randomY] = "X";
						existe = false;
						contador++;
					} else if (contador == 10) {
						existe = false;
					}
				} while (existe);
				

				for (int i = 0; i < tablero.length; i++) {
					for (int j = 0; j < tablero[i].length; j++) {
						System.out.print(tablero[i][j] + " ");
						if (j == tablero[i].length - 1) {
							System.out.println();
						}
					}
				}
			}

			// X
			// horizontales
			if (tablero[0][0] == "X" && tablero[0][1] == "X" && tablero[0][2] == "X") {
				System.out.println("Ha ganado el jugador X");
				victoria = false;
			} else if (tablero[1][0] == "X" && tablero[1][1] == "X" && tablero[1][2] == "X") {
				System.out.println("Ha ganado el jugador X");
				victoria = false;
			} else if (tablero[2][0] == "X" && tablero[2][1] == "X" && tablero[2][2] == "X") {
				System.out.println("Ha ganado el jugador X");
				victoria = false;
			}
			// verticales
			if (tablero[0][0] == "X" && tablero[1][0] == "X" && tablero[2][0] == "X") {
				System.out.println("Ha ganado el jugador X");
				victoria = false;
			} else if (tablero[0][1] == "X" && tablero[1][1] == "X" && tablero[2][1] == "X") {
				System.out.println("Ha ganado el jugador X");
				victoria = false;
			} else if (tablero[0][2] == "X" && tablero[1][2] == "X" && tablero[2][2] == "X") {
				System.out.println("Ha ganado el jugador X");
				victoria = false;
			}
			// diagonales
			if (tablero[0][0] == "X" && tablero[1][1] == "X" && tablero[2][2] == "X") {
				System.out.println("Ha ganado el jugador X");
				victoria = false;
			} else if (tablero[2][0] == "X" && tablero[1][1] == "X" && tablero[0][2] == "X") {
				System.out.println("Ha ganado el jugador X");
				victoria = false;
			} else

			// 0
			// horizontales
			if (tablero[0][0] == "0" && tablero[0][1] == "0" && tablero[0][2] == "0") {
				System.out.println("Ha ganado el jugador 0");
				victoria = false;
			} else if (tablero[1][0] == "0" && tablero[1][1] == "0" && tablero[1][2] == "0") {
				System.out.println("Ha ganado el jugador 0");
				victoria = false;
			} else if (tablero[2][0] == "0" && tablero[2][1] == "0" && tablero[2][2] == "0") {
				System.out.println("Ha ganado el jugador 0");
				victoria = false;
			} else
			// verticales
			if (tablero[0][0] == "0" && tablero[1][0] == "0" && tablero[2][0] == "0") {
				System.out.println("Ha ganado el jugador 0");
				victoria = false;
			} else if (tablero[0][1] == "0" && tablero[1][1] == "0" && tablero[2][1] == "0") {
				System.out.println("Ha ganado el jugador 0");
				victoria = false;
			} else if (tablero[0][2] == "0" && tablero[1][2] == "0" && tablero[2][2] == "0") {
				System.out.println("Ha ganado el jugador 0");
				victoria = false;
			} else
			// diagonales
			if (tablero[0][0] == "0" && tablero[1][1] == "0" && tablero[2][2] == "0") {
				System.out.println("Ha ganado el jugador 0");
				victoria = false;
			} else if (tablero[2][0] == "0" && tablero[1][1] == "0" && tablero[0][2] == "0") {
				System.out.println("Ha ganado el jugador 0");
				victoria = false;
			} else if (contador == 10) {
				System.out.println("Empate");
				victoria = false;
			}
		} while (victoria);

		input.close();
	}

}